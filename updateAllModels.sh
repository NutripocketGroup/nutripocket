#!/bin/bash
# Mapeado inverso de la base de datos en todos los modulos


sequelize-auto -o './users.module/src/db/models' -d nutripocket -h localhost -u root -p 3306 -x 1234 -e mysql -l ts
sequelize-auto -o './diets.module/src/db/models' -d nutripocket -h localhost -u root -p 3306 -x 1234 -e mysql -l ts
sequelize-auto -o './gateway.module/src/db/models' -d nutripocket -h localhost -u root -p 3306 -x 1234 -e mysql -l ts
sequelize-auto -o './foods.module/src/db/models' -d nutripocket -h localhost -u root -p 3306 -x 1234 -e mysql -l ts
