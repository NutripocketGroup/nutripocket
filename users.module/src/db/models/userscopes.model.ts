import {
  Sequelize,
  DataTypes,
  Model,
  Optional
} from "sequelize";


export interface UserScopes {
  id?: number;
  useremail: string;
  scopeid: number;
}

export type UserScopesCreationAttributes = Optional<
  UserScopes,
  "id" | "useremail" | "scopeid"
>;

export class UserScopesModel
  extends Model<UserScopes, UserScopesCreationAttributes>
  implements UserScopes
{
  public id?: number;
  public useremail: string;
  public scopeid: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}


export default function (sequelize: Sequelize): typeof UserScopesModel {
  UserScopesModel.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      useremail: {
        type: DataTypes.STRING(500),
        allowNull: false,
        references: {
          model: {
            tableName: "user",
          },
          key: "email",
        },
      },
      scopeid: {
        type: DataTypes.BIGINT,
        allowNull: false,
        references: {
          model: {
            tableName: "scopes",
          },
          key: "id",
        },
      },
    },
    {
      sequelize,
      tableName: "userscopes",
      timestamps: true,
      
    }
  );
  return UserScopesModel;
}
