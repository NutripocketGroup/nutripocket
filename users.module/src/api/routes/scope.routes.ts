import express from "express";
import { UserController } from "../controllers/user.controller";


//JWT Strategy
//import "../utils/auth/strategies/jwt";

const router = express.Router();
const userController = new UserController();


router.get(
  "/:id",
  // passport.authenticate("jwt", { session: false }),
  //  scopesValidationHandler(["read:users"]),
  userController.findUser
);

// Update a User with id
router.put(
  "/:id",

  /*     passport.authenticate("jwt", { session: false }),
scopesValidationHandler(["update:users"]),*/
  userController.updateUser

);

/*  router.delete("/:id",
   passport.authenticate("jwt", {session: false}),
  scopesValidationHandler(['delete:users']), 
  userController.delete);  */
/* } */


export default router;