import { UserScopesController } from './../controllers/userscopes.controller';
import express from "express";


const router = express.Router();
const userScopesController = new UserScopesController;


router.post(
    "/",
    /*     passport.authenticate("jwt", { session: false }),
        scopesValidationHandler(["read:users"]),*/
    userScopesController.createUserScope
);

router.post(
    "/",
    /*     passport.authenticate("jwt", { session: false }),
        scopesValidationHandler(["read:users"]),*/
    userScopesController.createUserScopes
);



router.get(
    "/byuser/:email",
    /*     passport.authenticate("jwt", { session: false }),
        scopesValidationHandler(["read:users"]),*/
    userScopesController.findScopesByUser
);

// Update a User with id
router.get(
    "/:id",

    /*     passport.authenticate("jwt", { session: false }),
  scopesValidationHandler(["update:users"]),*/
    userScopesController.findScopeByPk

);

router.delete("/:id",
    /*    passport.authenticate("jwt", {session: false}),
      scopesValidationHandler(['delete:users']),  */
    userScopesController.deleteScopeById

);  

/* module.exports = userRoutes; */

export default router;
