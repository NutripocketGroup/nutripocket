import express from "express";
import { UserController } from "../controllers/user.controller";

const router = express.Router();
const userController =  new UserController;


// Retrieve a single User with id
router.get(
  "/:id",
  /*     passport.authenticate("jwt", { session: false }),
      scopesValidationHandler(["read:users"]),*/
      userController.findUser
);

// Update a User with id
router.put(
  "/:id",

  /*     passport.authenticate("jwt", { session: false }),
scopesValidationHandler(["update:users"]),*/
  userController.updateUser

);

/*  router.delete("/:id",
   passport.authenticate("jwt", {session: false}),
  scopesValidationHandler(['delete:users']), 
  userController.delete);  */
/* } */
/* module.exports = userRoutes; */

export default router;
