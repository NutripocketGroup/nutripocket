import express from "express";
import { AuthController } from "../controllers/auth.controller";


//JWT Strategy
import "../../utils/auth/strategies/jwt";


const router = express.Router();
const authController = new AuthController();

/**
 * @swagger
 * /auth/sign-in:
 *   post:
 *    description: Sign in user
 *  
 */
router.post(
    "/sign-in",
    /* emailValidationHandler(), */ authController.signIn
);

router.post(
    "/social-sign-in",
    /* emailValidationHandler(),  */ authController.signIn
);

router.post(
    "/sign-up",
    /* emailValidationHandler(), */ authController.signUp
);

router.post(
    "/social-sign-up",
    /* emailValidationHandler(), */ authController.signUp
);


export default router;