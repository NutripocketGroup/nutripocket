

/*import {Op} from "sequelize";
 const Op = db.Sequelize.Op; */

import db from "../../db/config";
import { Scopes } from "../../db/models/scopes.model";

export class ScopesService {
  public scopes = db.Scopes;

  // Singleton constructor
  private static instance: ScopesService;
  private constructor() {}
  public static getInstance(): ScopesService {
    if (!ScopesService.instance) {
      ScopesService.instance = new ScopesService();
    }
    return ScopesService.instance;
  }

  async createScope(scope: Scopes) {
    try {
      return this.scopes.create(scope);
    } catch (error) {
      throw error;
    }
    
  }

  async createScopes(scopesArray: Scopes[]) {
    return this.scopes.bulkCreate(scopesArray);
  }

  async findAllScopesByRol(role: string) {
    return this.scopes.findAll({
      where: {
        role: role,
      },
    });
  }

  async updateScope(scope: Scopes, id: number) {
    return this.scopes.update(scope, {
      where: { id: id },
    });
  }

  async deleteScope(id: number) {
    return this.scopes.destroy({
      where: { id: id },
    });
  }
}
