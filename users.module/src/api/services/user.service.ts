import  DB  from './../../db/config';
import * as bcrypt from "bcryptjs";
import { ScopesModel } from '../../db/models/scopes.model';
import { User } from '../../db/models/user.model';
import { UserScopesModel } from '../../db/models/userscopes.model';


/*import {Op} from "sequelize";
 const Op = db.Sequelize.Op; */



export class UserService {
  public user = DB.User;
  // Singleton constructor
  private static instance: UserService;
  private constructor() {}
  public static getInstance(): UserService {
    if (!UserService.instance) {
      UserService.instance = new UserService();
    }
    return UserService.instance;
  }

  async createUserWithEmail(user: User): Promise<User> {
    try {
      if (!user.password) {
        throw new Error("Password is required");
      }
      user.password = bcrypt.hashSync(user.password, 10);
      user.role = "user";
      return this.user.create(user);
      
    } catch (error) {
      throw error;
    }
  
  }

  async createUserWithSocialLogin(user: User): Promise<User> {
    user.role = "user";
    return this.user.create(user);
  }

  async findAllUser(): Promise<User[]> {
    return this.user.findAll({
      attributes: ["email", "name","surname", "role", "birth","genre","heightcm","startweight","goalweight","typeofdiet","createdAt", "updatedAt","password"],
      include: [
        {
          model: UserScopesModel,
          attributes: ["id"],
          as: "scopes",
          
          include: [
            {
              model: ScopesModel,
              attributes: ["id", "name"],
              as: "scope",
            },
          ],
        },
      ],
    });
  }

  async findUserById(email: string) {
    return this.user.findByPk(email, {
      attributes: ["email", "name","surname", "role", "birth","genre","heightcm","startweight","goalweight","typeofdiet","createdAt", "updatedAt","password"],
      include: [
        {
          model: UserScopesModel,
          attributes: ["id"],
          isAliased: true,
          as: "userScopes",
          include: [
            {
              model: ScopesModel,
              attributes: ["id", "name"],
              isAliased: true,
              as: "scope",
            },
          ],
        },
      ],
    });
  }

  async updateUser(user:User, email:string) {
    return this.user.update(user, {
      where: { email: email },
    });
  }

  async deleteUser(email) {
    return this.user.destroy({
      where: { email: email },
    });
  }
}
