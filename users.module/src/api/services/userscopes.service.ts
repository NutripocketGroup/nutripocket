import db from "../../db/config";
import { ScopesModel } from "../../db/models/scopes.model";
import { UserScopes } from "../../db/models/userscopes.model";
/*import {Op} from "sequelize";
 const Op = db.Sequelize.Op; */


export class UserScopesService {
  public userscopes = db.UserScopes;
  //Singleton constructor
  private static instance: UserScopesService;
  private constructor() {}
  public static getInstance(): UserScopesService {
    if (!UserScopesService.instance) {
      UserScopesService.instance = new UserScopesService();
    }
    return UserScopesService.instance;
  }

  async findScopeByUser(id: number) {
    return this.userscopes.findByPk(id, {
      include: [
        {
          model: ScopesModel,
          attributes: ["id", "name"],
        },
      ],
    });
  }

  async createUserScope(userScope: UserScopes) {
    return this.userscopes.create(userScope);
  }

  async createUserScopes(userScopesArray: UserScopes[]) {
    return this.userscopes.bulkCreate(userScopesArray);
  }

  async findAllScopesByUser(email: string) {
    return this.userscopes.findAll({
      where: {
        useremail: email,
      },

      include: [
        {
          model: ScopesModel,
          attributes: ["id", "name"],
        },
      ],
    });
  }

  async deleteUserScope(id: number) {
    return this.userscopes.destroy({
      where: { id },
    });
  }
}
