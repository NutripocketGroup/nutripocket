import { User, UserModel } from "./../../db/models/user.model";

import { Request, Response, NextFunction } from "express";
import passport from "passport";
import jwt from "jsonwebtoken";
import * as moment from "moment";

const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../../config/config")[env];

//Basic strategy

import "../../utils/auth/strategies/basic";
import { UserScopes } from "../../db/models/userscopes.model";
import { ScopesService } from "../services/scopes.service";
import { UserScopesService } from "../services/userscopes.service";
import { UserService } from "../services/user.service";

export class AuthController {
  scopesService = ScopesService.getInstance();
  userScopesService = UserScopesService.getInstance();
  userService = UserService.getInstance();

  signIn = (req: Request, res: Response, next: NextFunction) => {
    passport.authenticate("basic", function (error, user: UserModel) {
      if (error || !user) {
        next(
          res.status(401).json({
            message: "Invalid username or password",
          })
        );
      }
      req.login(user, { session: false }, async function (error) {
        if (error) {
          next(
            res.status(400).json({
              message: error + ", Some error occurred loging user.",
            })
          );
        }

        if (!user) {
          next(
            res.status(401).json({
              message: "Unauthorized",
            })
          );
        }
        const { email, name } = user;
        const expireTime: number = 43800;
        const expireDate = moment.utc().add(expireTime, "minutes");
        const payload = {
          email: email,
          name: name,
          scopes: user["userScopes"].map((userscope: UserScopes[]) => {
            return userscope["scope"].name;
          }),
          tokenExpiresIn: expireDate,
        };

        const token = jwt.sign(payload, config.jwtSecret, {
          expiresIn: expireTime + "m",
        });

        return res.status(200).json({ token, user: { email, name } });
      })

    })(req, res, next);
  };

  signUp = (req: Request, res: Response) => {
    const reqUser: User = req.body;
    let createdUser: User;
    let userScopes: UserScopes[] = [];

    //Check if user already exists
    this.userService.findUserById(reqUser.email).then((user: UserModel) => {
      if (user) {
        res.status(409).json({
          message: "User already exists",
        });
        return;

      } 
      else 
      {
        this.userService
          .createUserWithEmail(reqUser)
          .then(async (user) => {
            createdUser = user;
            await this.scopesService
              .findAllScopesByRol(createdUser.role)
              .then((scopes) => {
                scopes.forEach((scope) => {
                  const createdUserScope: UserScopes = {
                    useremail: createdUser.email,
                    scopeid: scope.id,
                  };
                  userScopes.push(createdUserScope);
                });
              })
              .then(() => {
                this.userScopesService
                  .createUserScopes(userScopes)
                  .then(() => {
                    res.status(201).json({
                      data: createdUser.email,
                      message: "User created",
                    });
                  })
                  .catch((error) => {
                    res.status(500).json({
                      message: "Error creating user scopes",
                      error: error,
                    });
                  });
              })
              .catch((error) => {
                res.status(500).json({
                  message: "Error creating user scopes",
                  error
                });
              });
          })
          .catch((error) => {
            res.status(500).json({
              message: "Error creating user",
              error,
            });
          });
      }
    }
    ).catch((error) => {
      res.status(500).json({
        message: "Error checking if user exists",
        error: error,
      });
    }
    );
  }

}
