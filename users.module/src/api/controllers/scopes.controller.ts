import {Scopes} from '../../db/models/scopes.model';
import { Request, Response } from 'express'
import { ScopesService } from '../services/scopes.service';



export class ScopesController {

    scopesService = ScopesService.getInstance();

    createScope = (req: Request, res: Response) => {

        if (!req.body.scope || req.body.scope.role || req.body.scope.name) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }
        const scope: Scopes = req.body.scope;

        this.scopesService.createScope(scope)
            .then(data => {
                res.status(201).json({
                    message: "Scope created successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error creating scope",
                    error: err
                });
            });
    };

    findAllScopesByRol = (req: Request, res: Response) => {
        const rol: string = req.params.rol;
        if (!rol) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        this.scopesService.findAllScopesByRol(rol)
            .then(data => {
                res.status(200).json({
                    message: "Scopes retrieved successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error retrieving scopes",
                    error: err
                });
            });
    };



    updateScope = (req: Request, res: Response) => {
        const id: number = +req.params.id;
        const scope: Scopes = req.body.scope;

        if (!id || !scope) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        this.scopesService.updateScope(scope, id)
            .then((count) => {
                if (count.length == 1) {
                    res.json({ message: "Scope updated successfully." }).status(201);
                } else {
                    res.json({
                        message:
                            `Cannot update =${scope} scope. Maybe was not found or request body is empty!`
                    }
                    );
                }
            })
            .catch((err) => {
                res.status(500).json({ message: " Some error occurred while updating scope " + scope ,
                    error: err
                })
            });
    };


    delete = (req: Request, res: Response) => {
        const id: number = +req.params.id;
        if (!id) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        this.scopesService
            .deleteScope(id)
            .then((num) => {
                if (num == 1) {
                    res.status(200).json({
                        message: "Scope was deleted successfully!",
                    });
                } else {
                    res.json({
                        message: `Cannot delete scope with id ${id}. Maybe was not found!`,
                    });
                }
            })
            .catch((err) => {
                res.status(500).json({
                    message: " Somethinsg ocurred while deleting scope with id " + id,
                    error: err
                });
            });
    };
}