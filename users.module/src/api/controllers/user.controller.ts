import { User, UserModel } from './../../db/models/user.model';
import { Request, Response } from 'express';
import { UserService } from '../services/user.service';




export class UserController {

    userService = UserService.getInstance();


    // Create and Save a new user
    createUserWithEmail = (req: Request, res: Response) => {

        if (!req.body.user || req.body.user.email || req.body.user.password || req.body.user.name) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        const user: User = req.body.user;


        this.userService.createUserWithEmail(user)
            .then(data => {
                res.status(201).json({
                    message: "User created successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error creating user",
                    error: err
                });
            });
    };

    findAllUsers = (_req: Request, res: Response) => {
        this.userService.findAllUser()
            .then(data => {
                
                res.status(200).json(data);
            })
            .catch(err => {
                res.status(500).json({
                    message:" Some error occurred while retrieving users.",
                    error: err
                });
            });
    };



    findUser = (req: Request, res: Response) => {

        const id: string = req.params.id;
        if (!id) {
            return res.status(400).json({ message: "Missing id" });
        }
        return this.userService.findUserById(id)
            .then((data) => {
                let resUser:UserModel=data;
                resUser.password=null;
                res.json({
                    message: "User retrieved successfully",
                    data: resUser
                }).status(200);
            })
            .catch((err) => {
                res.json({
                    message: "Error retrieving user with id " + id,
                    error: err
                })
            });

    };

    updateUser = (req: Request, res: Response) => {
        this.userService
            .updateUser(req.body, req.params.id)
            .then((count) => {
                if (count[0] == 1) {
                    res.json({ message: "User updated successfully." }).status(201);
                } else {
                    res.status(404).json({
                        message:
                            `Cannot update User with email=${req.params.id}. Maybe User was not found or request body is empty!`,
                    }
                    );
                }
            })
            .catch((err) => {
                res.status(500).json({
                    message: " Some error occurred while updating user " + req.params.id,
                    error: err
                });
            });
    };

    delete = (req: Request, res: Response) => {
        this.userService
            .deleteUser(req.params.email)
            .then((num) => {
                if (num == 1) {
                    res.status(200).json({
                        message: "User was deleted successfully!",
                    });
                } else {
                    res.json({
                        message: "Cannot delete User with email=" + req.params.email + ". Maybe User was not found or request body is empty!"
                    });
                }
            })
            .catch((err) => {
                res.status(500).json({
                    message: "Could not delete User with email=" + req.params.email,
                    error: err
                });
            });
    };
}
