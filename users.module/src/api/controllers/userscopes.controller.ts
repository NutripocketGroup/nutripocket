import { UserScopes } from './../../db/models/userscopes.model';

import { Request, Response } from "express";
import { UserScopesService } from '../services/userscopes.service';
;


export class UserScopesController {
    //Singleton

    userScopesService = UserScopesService.getInstance();



    // Create and Save a new user
    createUserScope = (req: Request, res: Response) => {

        if (!req.body.userScopes.user || req.body.userScopes.scope) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        const userScopes: UserScopes = req.body.userScopes;

        this.userScopesService.createUserScope(userScopes)
            .then(data => {
                res.status(201).json({
                    message: "UserScopes created successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error creating userScopes",
                    error: err
                });
            });
    };

    createUserScopes = (req: Request, res: Response) => {

        if (!req.body.userScopes.user || req.body.userScopes.scope) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }

        const userScopes: UserScopes[] = req.body.userScopes;

        this.userScopesService.createUserScopes(userScopes)
            .then(data => {
                res.status(201).json({
                    message: "UserScopes created successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error creating userScopes",
                    error: err
                });
            });
    };

    findScopesByUser = (req: Request, res: Response) => {

        if (!req.params.email) {
            res.status(400).json({
                message: "Content cannot be empty!"
            });
            return;
        }
        const user: string = req.body.user

        this.userScopesService.findAllScopesByUser(user)
            .then(data => {
                res.status(200).json({
                    message: "Scopes retrieved successfully",
                    data: data
                });
            })
            .catch(err => {
                res.status(500).json({
                    message: "Error retrieving userScopes",
                    error: err
                });
            });
    };


    findScopeByPk = (req: Request, res: Response) => {
        const id: number = +req.params.id;
        if (!id) {
            return res.status(400).json({ message: "Missing id" });
        }
        return this.userScopesService.findScopeByUser(id)
            .then((data) => {
                return res.json({
                    message: "Scope retrieved successfully",
                    data: data
                }).status(200);
            })
            .catch((err) => {
                res.json({ message: "Error retrieving userScopes", error: err }).status(500);
            });
    };

    deleteScopeById = (req: Request, res: Response) => {

        if (!req.params.id) {
            res.status(400).json({ message: "Missing id" });
        }

        const id = +req.params.id;

        this.userScopesService
            .deleteUserScope(id)
            .then((num) => {
                if (num == 1) {
                    res.status(200).json({
                        message: "Scope was deleted successfully!",
                    });
                } else {
                    res.json({
                        message: `Cannot delete Scope with id=${id}. Maybe was not found!`,
                    });
                }
            })
            .catch((err) => {
                res.status(500).json({
                    message: "Error deleting userScopes",
                    error: err
                });
            })
    }
}