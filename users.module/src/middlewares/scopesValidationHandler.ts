
import { Request, Response, NextFunction } from 'express';
import { User} from "../db/models/user.model";


export function scopesValidationHandler(allowedScopes: string[]) {
  return function (req:Request, res:Response, next:NextFunction) {
    
    const user = req.user as User;
    
    if (!req.user || (req.user && !req.user)) {
      next(res.status(401).json({ message: "Missing scopes" }));
    }

    const hasAccess = allowedScopes.some(scope => user["userScopes"].map((userscopescope)=>{return userscopescope.scope.name}).includes(scope));

    if (hasAccess) {
      next();
    } else {
      next(res.status(401).json({ message: "Missing scopes" }));
    }
  };
}

