import passport from "passport";
import boom from "@hapi/boom";
import { Strategy, ExtractJwt } from "passport-jwt";
import { UserService } from "../../../api/services/user.service";

const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../../../config/config")[env];

passport.use(
  new Strategy(
    {
      secretOrKey: config.jwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
    async function (tokenPayload, cb) {
      const userService = UserService.getInstance();

      try {
        const user = await userService.findUserById(tokenPayload.email);
        if (!user) {
          return cb(boom.unauthorized("User missing"));
        }

        user.password = "";

        cb(null, user, { scopes: tokenPayload.scopes });
      } catch (error: any) {
        return cb(boom.unauthorized("Something went wrong " + error));
      }
    }
  )
);
