import  {validateEmail } from '../../../api/contracts/email-validation.contract';
import { BasicStrategy } from "passport-http";
import passport from "passport";
import bcrypt from "bcryptjs";
import boom from "@hapi/boom";
import { UserService } from "../../../api/services/user.service";

passport.use(
  new BasicStrategy(function (username: string, password: string, cb) {
    const userService = UserService.getInstance();
    
    if(!validateEmail(username)) {
      return cb(boom.badRequest('Invalid email'));
    }

    try {
      userService.findUserById(username).then((user) => {
        if (!user || !user.password) {
          cb(boom.unauthorized(), false);
        }

        if (!bcrypt.compareSync(password, user.password)) {
          cb(boom.unauthorized(), false);
        }

        delete user.password;

        cb(null, user);
      });
    } catch (error) {
      return cb(error);
    }
  })
);
