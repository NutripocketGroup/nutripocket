import express from "express";

import passport from "passport";
import morgan from "morgan";
import cors from "cors";

import userRoutes from "./api/routes/scope.routes";
import authRoutes from "./api/routes/auth.routes";
import scopeRoutes from "./api/routes/scope.routes";
import userScopesRoutes from "./api/routes/userscopes.routes";
import db from "./db/config";



//Declare app express server
const app = express();
//Swagger?



//Initialize database
db.sequelize.sync({ force: false });

app.use(express.json());

app.use(cors());

//routes
app.use("/api/user", userRoutes);
app.use("/auth", authRoutes);
app.use("/api/scope", scopeRoutes);
app.use("/api/userscopes", userScopesRoutes);


const PORT = process.env.PORT || 3001;

app.use(morgan("dev")); //tiny
app.use(passport.initialize());
try {
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
} catch (error: any) {
  console.error(`Error ocurred: ${error.message}`);
}
