# syntax=docker/dockerfile:1

#compile
FROM maven:3.8.4-openjdk-17 as build-step

COPY . .

RUN mvn clean -DskipTests=true

RUN mvn install -DskipTests=true

CMD ["mvn","spring-boot:run"]
