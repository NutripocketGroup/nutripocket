package com.nutripocket.nutripocket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class NutripocketApplication {

	public static void main(String[] args) {
		SpringApplication.run(NutripocketApplication.class, args);
	}

}
