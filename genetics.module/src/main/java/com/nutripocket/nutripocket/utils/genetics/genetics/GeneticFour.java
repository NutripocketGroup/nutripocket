package com.nutripocket.nutripocket.utils.genetics.genetics;


import com.nutripocket.nutripocket.models.entities.Food;
import com.nutripocket.nutripocket.models.dto.DishTotals;
import com.nutripocket.nutripocket.models.dto.DishWithScoreFourItems;
import com.nutripocket.nutripocket.models.dto.ItemDto;
import com.nutripocket.nutripocket.models.dto.Requirements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class GeneticFour {

    //Parameters of genetic algorithm
    private Double randomLimit;

    private int populationSize = 50;
    private int maxIterations = 50;
    private Double caloriesWeightPercent = 70.0;
    private Double proteinWeightPercent = 20.0;
    private Double fatWeightPercent = 5.0;
    private Double carbsWeightPercent = 5.0;



    private  List<DishWithScoreFourItems> generateOrAddRandomPopulationX4Foods(Requirements requirements, List<Food> foods,
                                                                               List<DishWithScoreFourItems> existentPopulation) {
        List<DishWithScoreFourItems> dishesWithScoreArray=new ArrayList<>();
        randomLimit = requirements.getCaloriesburned() / 4;
        if (existentPopulation.size() > 0) {
            List<DishWithScoreFourItems> finalDishesWithScoreArray = dishesWithScoreArray;
            dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
                finalDishesWithScoreArray.add(dishWithScoreFourItems);
            });
        }

            for (int i = dishesWithScoreArray.size(); i < populationSize; i++) {
                DishWithScoreFourItems dishWithScore = new DishWithScoreFourItems();
                ItemDto item_Dto_1 = new ItemDto();
                item_Dto_1.setFood(foods.get(0));
                item_Dto_1.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
                ItemDto item_Dto_2 = new ItemDto();
                item_Dto_2.setFood(foods.get(1));
                item_Dto_2.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
                ItemDto item_Dto_3 = new ItemDto();
                item_Dto_3.setFood(foods.get(2));
                item_Dto_3.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
                ItemDto item_Dto_4 = new ItemDto();
                item_Dto_4.setFood(foods.get(3));
                item_Dto_4.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));

                dishWithScore.setItem_Dto_1(item_Dto_1);
                dishWithScore.setItem_Dto_2(item_Dto_2);
                dishWithScore.setItem_Dto_3(item_Dto_3);
                dishWithScore.setItem_Dto_4(item_Dto_4);
                dishWithScore.setPunctuation(0.0);
                dishesWithScoreArray.add(dishWithScore);
            }

        return dishesWithScoreArray;
    }


    //Fitness function
    private DishWithScoreFourItems fitnessOne(DishWithScoreFourItems dishWithScoreFourItems, Requirements requirements) {
            DishTotals totals = calculateTotals(dishWithScoreFourItems);
            Double caloriesPunctuation = Math
                    .abs((totals.getTotalCalories() / requirements.getCaloriesburned() * caloriesWeightPercent)
                            - caloriesWeightPercent);

            Double proteinPunctuation = Math
                    .abs((totals.getTotalProtein() / requirements.getProtein_req()
                            * proteinWeightPercent)
                            - proteinWeightPercent);

            Double carbsPunctuation = Math.abs(
                    (totals.getTotalCarbs() / requirements.getCarbs_req() * carbsWeightPercent)
                            - carbsWeightPercent);

            Double fatPunctuation = Math
                    .abs((totals.getTotalFat() / requirements.getFat_req() * fatWeightPercent)
                            - fatWeightPercent);

            Double punctuation = caloriesPunctuation + proteinPunctuation + carbsPunctuation
                    + fatPunctuation;
            dishWithScoreFourItems.setPunctuation(punctuation);

            return dishWithScoreFourItems;
    }

    private List<DishWithScoreFourItems> fitnessList(List<DishWithScoreFourItems> dishesWithScoreArray, Requirements requirements) {
        List<DishWithScoreFourItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray.add(fitnessOne(dishWithScoreFourItems, requirements));
        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreFourItems> orderByPunctuation(List<DishWithScoreFourItems> dishesWithScoreArray) {
        List<DishWithScoreFourItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray.add(dishWithScoreFourItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreFourItems, t1) -> {
            return dishWithScoreFourItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        return finalDishesWithScoreArray;
    }

 private List<DishWithScoreFourItems> holdOnlyFirst10(List<DishWithScoreFourItems> dishesWithScoreArray) {
        List<DishWithScoreFourItems> finalDishesWithScoreArray = new ArrayList<>();
     List<DishWithScoreFourItems> finalDishesWithScoreArray1 = finalDishesWithScoreArray;
     dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray1.add(dishWithScoreFourItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreFourItems, t1) -> {
            return dishWithScoreFourItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        finalDishesWithScoreArray = finalDishesWithScoreArray.subList(0, 10);
        return finalDishesWithScoreArray;
 }



    private DishTotals calculateTotals(DishWithScoreFourItems dishWithScoreFourItems) {
        DishTotals totals = new DishTotals();


        Double item1TotalCalories = ( (dishWithScoreFourItems.getItem_Dto_1().getFood().getCalories100G()/100) *
                dishWithScoreFourItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalProteins = ( (dishWithScoreFourItems.getItem_Dto_1().getFood().getProtein100G()/100) *
                dishWithScoreFourItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalCarbs = ( (dishWithScoreFourItems.getItem_Dto_1().getFood().getCarbs100G()/100) *
                dishWithScoreFourItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalFat = ( (dishWithScoreFourItems.getItem_Dto_1().getFood().getFat100G()/100) *
                dishWithScoreFourItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_1().getFood().getDoseg());

        Double item2TotalCalories = ( (dishWithScoreFourItems.getItem_Dto_2().getFood().getCalories100G()/100) *
                dishWithScoreFourItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalProteins = ( (dishWithScoreFourItems.getItem_Dto_2().getFood().getProtein100G()/100) *
                dishWithScoreFourItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalCarbs = ( (dishWithScoreFourItems.getItem_Dto_2().getFood().getCarbs100G()/100) *
                dishWithScoreFourItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalFat = ( (dishWithScoreFourItems.getItem_Dto_2().getFood().getFat100G()/100) *
                dishWithScoreFourItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_2().getFood().getDoseg());

        Double item3TotalCalories = ( (dishWithScoreFourItems.getItem_Dto_3().getFood().getCalories100G()/100) *
                dishWithScoreFourItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalProteins = ( (dishWithScoreFourItems.getItem_Dto_3().getFood().getProtein100G()/100) *
                dishWithScoreFourItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalCarbs = ( (dishWithScoreFourItems.getItem_Dto_3().getFood().getCarbs100G()/100) *
                dishWithScoreFourItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalFat = ( (dishWithScoreFourItems.getItem_Dto_3().getFood().getFat100G()/100) *
                dishWithScoreFourItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_3().getFood().getDoseg());

        Double item4TotalCalories = ( (dishWithScoreFourItems.getItem_Dto_4().getFood().getCalories100G()/100) *
                dishWithScoreFourItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalProteins = ( (dishWithScoreFourItems.getItem_Dto_4().getFood().getProtein100G()/100) *
                dishWithScoreFourItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalCarbs = ( (dishWithScoreFourItems.getItem_Dto_4().getFood().getCarbs100G()/100) *
                dishWithScoreFourItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalFat = ( (dishWithScoreFourItems.getItem_Dto_4().getFood().getFat100G()/100) *
                dishWithScoreFourItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFourItems.getItem_Dto_4().getFood().getDoseg());


        totals.setTotalCalories(item1TotalCalories + item2TotalCalories + item3TotalCalories + item4TotalCalories);
        totals.setTotalProtein(item1TotalProteins + item2TotalProteins + item3TotalProteins + item4TotalProteins);
        totals.setTotalCarbs(item1TotalCarbs + item2TotalCarbs + item3TotalCarbs + item4TotalCarbs);
        totals.setTotalFat(item1TotalFat + item2TotalFat + item3TotalFat + item4TotalFat);

        return totals;
    }

    private DishWithScoreFourItems getOnlyFirstDish(List<DishWithScoreFourItems> dishesWithScoreArray) {
        DishWithScoreFourItems dishWithScoreFourItems = new DishWithScoreFourItems();
        dishWithScoreFourItems = dishesWithScoreArray.get(0);
        return dishWithScoreFourItems;
    }

    private List<Double> getBestsDosex(DishWithScoreFourItems dishesWithScore) {
        List<Double> bestDosex = new ArrayList<>();

            bestDosex.add(dishesWithScore.getItem_Dto_1().getDoseX());
            bestDosex.add(dishesWithScore.getItem_Dto_2().getDoseX());
            bestDosex.add(dishesWithScore.getItem_Dto_3().getDoseX());
            bestDosex.add(dishesWithScore.getItem_Dto_4().getDoseX());

        return bestDosex;
    }


    //generate 10 new mixing existing dishes randomly

    private List<DishWithScoreFourItems> generatecrossedDishes(List<DishWithScoreFourItems> dishesWithScoreArray) {
        List<DishWithScoreFourItems> crossedDishes = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            DishWithScoreFourItems dishWithScoreFourItems = new DishWithScoreFourItems();
            ItemDto itemDto1 = new ItemDto();
            ItemDto itemDto2 = new ItemDto();
            ItemDto itemDto3 = new ItemDto();
            ItemDto itemDto4 = new ItemDto();

            itemDto1.setFood(dishesWithScoreArray.get(i).getItem_Dto_1().getFood());
            itemDto2.setFood(dishesWithScoreArray.get(i).getItem_Dto_2().getFood());
            itemDto3.setFood(dishesWithScoreArray.get(i).getItem_Dto_3().getFood());
            itemDto4.setFood(dishesWithScoreArray.get(i).getItem_Dto_4().getFood());


            itemDto1.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_1().getDoseX());
            itemDto2.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_2().getDoseX());
            itemDto3.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_3().getDoseX());
            itemDto4.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_4().getDoseX());


            dishWithScoreFourItems.setItem_Dto_1(itemDto1);
            dishWithScoreFourItems.setItem_Dto_2(itemDto2);
            dishWithScoreFourItems.setItem_Dto_3(itemDto3);
            dishWithScoreFourItems.setItem_Dto_4(itemDto4);


            crossedDishes.add(dishWithScoreFourItems);
        }
        return crossedDishes;
    }

    public List<Double> geneticFourAlgorithm(List<Food> foods, Requirements requirements) {

        DishWithScoreFourItems finalDish = new DishWithScoreFourItems();
        List<DishWithScoreFourItems> population = new ArrayList<>();

        //genetic loop
        for (int i = 0; i < maxIterations; i++) {

            // Initialize population

            List<DishWithScoreFourItems> finalPopulation1 = population;

            generateOrAddRandomPopulationX4Foods(requirements, foods, population).stream().forEach(dishWithScoreFourItems -> {
                finalPopulation1.add(dishWithScoreFourItems);
            });


            //  fitness
            List<DishWithScoreFourItems> puntuatedPopulation = new ArrayList<>();
            fitnessList(population, requirements).stream().forEach(dishWithScoreFourItems -> {
                puntuatedPopulation.add(dishWithScoreFourItems);
            });


            //Order by fitness
            List<DishWithScoreFourItems> orderedPopulation = new ArrayList<>();
            orderByPunctuation(puntuatedPopulation).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation.add(dishWithScoreFourItems);
            });

            // Select bests
            List<DishWithScoreFourItems> bests = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation).stream().forEach(dishWithScoreFourItems -> {
                bests.add(dishWithScoreFourItems);
            });
            population = bests;


            // Generate crossed dishes
            List<DishWithScoreFourItems> crossedDishes = new ArrayList<>();
            generatecrossedDishes(bests).stream().forEach(dishWithScoreFourItems -> {
                crossedDishes.add(dishWithScoreFourItems);
            });


            // fitness
            List<DishWithScoreFourItems> puntuatedCrossedDishes = new ArrayList<>();
            fitnessList(crossedDishes, requirements).stream().forEach(dishWithScoreFourItems -> {
                puntuatedCrossedDishes.add(dishWithScoreFourItems);
            });


            //add crossed dishes to population
            List<DishWithScoreFourItems> finalPopulation2 = population;
            puntuatedCrossedDishes.stream().forEach(dishWithScoreFourItems -> {
                finalPopulation2.add(dishWithScoreFourItems);
            });

            //Order by fitness
            List<DishWithScoreFourItems> orderedPopulation2 = new ArrayList<>();
            orderByPunctuation(puntuatedCrossedDishes).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation2.add(dishWithScoreFourItems);
            });

            // Select bests
            List<DishWithScoreFourItems> bests2 = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation2).stream().forEach(dishWithScoreFourItems -> {
                bests2.add(dishWithScoreFourItems);
            });



            //add bests to final population

            for(int j = 0; j < 10; j++){
                population.add(bests2.get(j));
            }
            //Order by fitness
            List<DishWithScoreFourItems> orderedPopulation3 = new ArrayList<>();
            orderByPunctuation(population).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation3.add(dishWithScoreFourItems);
            });

            population=orderedPopulation3;


            finalDish = getOnlyFirstDish(population);

        }

        return getBestsDosex(finalDish);
    }
}
