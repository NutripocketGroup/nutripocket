package com.nutripocket.nutripocket.utils.genetics.genetics;

import com.nutripocket.nutripocket.models.entities.Food;
import com.nutripocket.nutripocket.models.dto.DishTotals;
import com.nutripocket.nutripocket.models.dto.DishWithScoreTwoItems;
import com.nutripocket.nutripocket.models.dto.ItemDto;
import com.nutripocket.nutripocket.models.dto.Requirements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class GeneticTwo {

    //Parameters of genetic algorithm
    private Double randomLimit;

    private int populationSize = 30;
    private int maxIterations = 30;
    private Double caloriesWeightPercent = 70.0;
    private Double proteinWeightPercent = 20.0;
    private Double fatWeightPercent = 5.0;
    private Double carbsWeightPercent = 5.0;



    private List<DishWithScoreTwoItems> generateOrAddRandomPopulationX4Foods(Requirements requirements, List<Food> foods,
                                                                             List<DishWithScoreTwoItems> existentPopulation) {
        List<DishWithScoreTwoItems> dishesWithScoreArray=new ArrayList<>();
        randomLimit = requirements.getCaloriesburned() / 4;
        if (existentPopulation.size() > 0) {
            List<DishWithScoreTwoItems> finalDishesWithScoreArray = dishesWithScoreArray;
            dishesWithScoreArray.stream().forEach(dishWithScoreTwoItems -> {
                finalDishesWithScoreArray.add(dishWithScoreTwoItems);
            });
        }

        for (int i = dishesWithScoreArray.size(); i < populationSize; i++) {
            DishWithScoreTwoItems dishWithScore = new DishWithScoreTwoItems();
            ItemDto item_Dto_1 = new ItemDto();
            item_Dto_1.setFood(foods.get(0));
            item_Dto_1.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_2 = new ItemDto();
            item_Dto_2.setFood(foods.get(1));
            item_Dto_2.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));


            dishWithScore.setItem_Dto_1(item_Dto_1);
            dishWithScore.setItem_Dto_2(item_Dto_2);
            dishWithScore.setPunctuation(0.0);
            dishesWithScoreArray.add(dishWithScore);
        }

        return dishesWithScoreArray;
    }


    //Fitness function
    private DishWithScoreTwoItems fitnessOne(DishWithScoreTwoItems dishWithScoreTwoItems, Requirements requirements) {
        DishTotals totals = calculateTotals(dishWithScoreTwoItems);
        Double caloriesPunctuation = Math
                .abs((totals.getTotalCalories() / requirements.getCaloriesburned() * caloriesWeightPercent)
                        - caloriesWeightPercent);

        Double proteinPunctuation = Math
                .abs((totals.getTotalProtein() / requirements.getProtein_req()
                        * proteinWeightPercent)
                        - proteinWeightPercent);

        Double carbsPunctuation = Math.abs(
                (totals.getTotalCarbs() / requirements.getCarbs_req() * carbsWeightPercent)
                        - carbsWeightPercent);

        Double fatPunctuation = Math
                .abs((totals.getTotalFat() / requirements.getFat_req() * fatWeightPercent)
                        - fatWeightPercent);

        Double punctuation = caloriesPunctuation + proteinPunctuation + carbsPunctuation
                + fatPunctuation;
        dishWithScoreTwoItems.setPunctuation(punctuation);

        return dishWithScoreTwoItems;
    }

    private List<DishWithScoreTwoItems> fitnessList(List<DishWithScoreTwoItems> dishesWithScoreArray, Requirements requirements) {
        List<DishWithScoreTwoItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreTwoItems -> {
            finalDishesWithScoreArray.add(fitnessOne(dishWithScoreTwoItems, requirements));
        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreTwoItems> orderByPunctuation(List<DishWithScoreTwoItems> dishesWithScoreArray) {
        List<DishWithScoreTwoItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreTwoItems -> {
            finalDishesWithScoreArray.add(dishWithScoreTwoItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreTwoItems, t1) -> {
            return dishWithScoreTwoItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreTwoItems> holdOnlyFirst10(List<DishWithScoreTwoItems> dishesWithScoreArray) {
        List<DishWithScoreTwoItems> finalDishesWithScoreArray = new ArrayList<>();
        List<DishWithScoreTwoItems> finalDishesWithScoreArray1 = finalDishesWithScoreArray;
        dishesWithScoreArray.stream().forEach(dishWithScoreTwoItems -> {
            finalDishesWithScoreArray1.add(dishWithScoreTwoItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreTwoItems, t1) -> {
            return dishWithScoreTwoItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        finalDishesWithScoreArray = finalDishesWithScoreArray.subList(0, 10);
        return finalDishesWithScoreArray;
    }



    private DishTotals calculateTotals(DishWithScoreTwoItems dishWithScoreTwoItems) {
        DishTotals totals = new DishTotals();

        Double item1TotalCalories = ( (dishWithScoreTwoItems.getItem_Dto_1().getFood().getCalories100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_1().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalProteins = ( (dishWithScoreTwoItems.getItem_Dto_1().getFood().getProtein100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_1().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalCarbs = ( (dishWithScoreTwoItems.getItem_Dto_1().getFood().getCarbs100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_1().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalFat = ( (dishWithScoreTwoItems.getItem_Dto_1().getFood().getFat100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_1().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_1().getFood().getDoseg());

        Double item2TotalCalories = ( (dishWithScoreTwoItems.getItem_Dto_2().getFood().getCalories100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_2().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalProteins = ( (dishWithScoreTwoItems.getItem_Dto_2().getFood().getProtein100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_2().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalCarbs = ( (dishWithScoreTwoItems.getItem_Dto_2().getFood().getCarbs100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_2().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalFat = ( (dishWithScoreTwoItems.getItem_Dto_2().getFood().getFat100G()/100) *
                dishWithScoreTwoItems.getItem_Dto_2().getDoseX() *
                dishWithScoreTwoItems.getItem_Dto_2().getFood().getDoseg());


        totals.setTotalCalories(item1TotalCalories + item2TotalCalories);
        totals.setTotalProtein(item1TotalProteins + item2TotalProteins);
        totals.setTotalCarbs(item1TotalCarbs + item2TotalCarbs);
        totals.setTotalFat(item1TotalFat + item2TotalFat);

        return totals;
    }

    private DishWithScoreTwoItems getOnlyFirstDish(List<DishWithScoreTwoItems> dishesWithScoreArray) {
        DishWithScoreTwoItems dishWithScoreTwoItems = new DishWithScoreTwoItems();
        dishWithScoreTwoItems = dishesWithScoreArray.get(0);
        return dishWithScoreTwoItems;
    }

    private List<Double> getBestsDosex(DishWithScoreTwoItems dishesWithScore) {
        List<Double> bestDosex = new ArrayList<>();

        bestDosex.add(dishesWithScore.getItem_Dto_1().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_2().getDoseX());

        return bestDosex;
    }


    //generate 10 new mixing existing dishes randomly

    private List<DishWithScoreTwoItems> generatecrossedDishes(List<DishWithScoreTwoItems> dishesWithScoreArray) {
        List<DishWithScoreTwoItems> crossedDishes = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            DishWithScoreTwoItems dishWithScoreThreeItems = new DishWithScoreTwoItems();
            ItemDto itemDto1 = new ItemDto();
            ItemDto itemDto2 = new ItemDto();



            itemDto1.setFood(dishesWithScoreArray.get(i).getItem_Dto_1().getFood());
            itemDto2.setFood(dishesWithScoreArray.get(i).getItem_Dto_2().getFood());


            itemDto1.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_1().getDoseX());
            itemDto2.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_2().getDoseX());


            dishWithScoreThreeItems.setItem_Dto_1(itemDto1);
            dishWithScoreThreeItems.setItem_Dto_2(itemDto2);


            crossedDishes.add(dishWithScoreThreeItems);
        }
        return crossedDishes;
    }

    public List<Double> geneticTwoAlgorithm(List<Food> foods, Requirements requirements) {

        DishWithScoreTwoItems finalDish = new DishWithScoreTwoItems();
        List<DishWithScoreTwoItems> population = new ArrayList<>();

        //genetic loop
        for (int i = 0; i < maxIterations; i++) {

            // Initialize population

            List<DishWithScoreTwoItems> finalPopulation1 = population;

            generateOrAddRandomPopulationX4Foods(requirements, foods, population).stream().forEach(dishWithScoreTwoItems -> {
                finalPopulation1.add(dishWithScoreTwoItems);
            });


            //  fitness
            List<DishWithScoreTwoItems> puntuatedPopulation = new ArrayList<>();
            fitnessList(population, requirements).stream().forEach(dishWithScoreTwoItems -> {
                puntuatedPopulation.add(dishWithScoreTwoItems);
            });


            //Order by fitness
            List<DishWithScoreTwoItems> orderedPopulation = new ArrayList<>();
            orderByPunctuation(puntuatedPopulation).stream().forEach(dishWithScoreTwoItems -> {
                orderedPopulation.add(dishWithScoreTwoItems);
            });

            // Select bests
            List<DishWithScoreTwoItems> bests = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation).stream().forEach(dishWithScoreTwoItems -> {
                bests.add(dishWithScoreTwoItems);
            });
            population = bests;


            // Generate crossed dishes
            List<DishWithScoreTwoItems> crossedDishes = new ArrayList<>();
            generatecrossedDishes(bests).stream().forEach(dishWithScoreTwoItems -> {
                crossedDishes.add(dishWithScoreTwoItems);
            });


            // fitness
            List<DishWithScoreTwoItems> puntuatedCrossedDishes = new ArrayList<>();
            fitnessList(crossedDishes, requirements).stream().forEach(dishWithScoreTwoItems -> {
                puntuatedCrossedDishes.add(dishWithScoreTwoItems);
            });


            //add crossed dishes to population
            List<DishWithScoreTwoItems> finalPopulation2 = population;
            puntuatedCrossedDishes.stream().forEach(dishWithScoreTwoItems -> {
                finalPopulation2.add(dishWithScoreTwoItems);
            });

            //Order by fitness
            List<DishWithScoreTwoItems> orderedPopulation2 = new ArrayList<>();
            orderByPunctuation(puntuatedCrossedDishes).stream().forEach(dishWithScoreTwoItems -> {
                orderedPopulation2.add(dishWithScoreTwoItems);
            });

            // Select bests
            List<DishWithScoreTwoItems> bests2 = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation2).stream().forEach(dishWithScoreTwoItems -> {
                bests2.add(dishWithScoreTwoItems);
            });



            //add bests to final population

            for(int j = 0; j < 10; j++){
                population.add(bests2.get(j));
            }
            //Order by fitness
            List<DishWithScoreTwoItems> orderedPopulation3 = new ArrayList<>();
            orderByPunctuation(population).stream().forEach(dishWithScoreTwoItems -> {
                orderedPopulation3.add(dishWithScoreTwoItems);
            });

            population=orderedPopulation3;


            finalDish = getOnlyFirstDish(population);

        }
        return getBestsDosex(finalDish);
    }
}
