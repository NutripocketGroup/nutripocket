package com.nutripocket.nutripocket.utils.genetics.genetics;


import com.nutripocket.nutripocket.models.entities.Food;
import com.nutripocket.nutripocket.models.dto.DishTotals;
import com.nutripocket.nutripocket.models.dto.DishWithScoreThreeItems;
import com.nutripocket.nutripocket.models.dto.ItemDto;
import com.nutripocket.nutripocket.models.dto.Requirements;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class GeneticThree {

    //Parameters of genetic algorithm
    private Double randomLimit;

    private int populationSize = 20;
    private int maxIterations = 20;
    private Double caloriesWeightPercent = 70.0;
    private Double proteinWeightPercent = 20.0;
    private Double fatWeightPercent = 5.0;
    private Double carbsWeightPercent = 5.0;



    private List<DishWithScoreThreeItems> generateOrAddRandomPopulationX4Foods(Requirements requirements, List<Food> foods,
                                                                               List<DishWithScoreThreeItems> existentPopulation) {
        List<DishWithScoreThreeItems> dishesWithScoreArray=new ArrayList<>();
        randomLimit = requirements.getCaloriesburned() / 4;
        if (existentPopulation.size() > 0) {
            List<DishWithScoreThreeItems> finalDishesWithScoreArray = dishesWithScoreArray;
            dishesWithScoreArray.stream().forEach(dishWithScorethreeItems -> {
                finalDishesWithScoreArray.add(dishWithScorethreeItems);
            });
        }

        for (int i = dishesWithScoreArray.size(); i < populationSize; i++) {
            DishWithScoreThreeItems dishWithScore = new DishWithScoreThreeItems();
            ItemDto item_Dto_1 = new ItemDto();
            item_Dto_1.setFood(foods.get(0));
            item_Dto_1.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_2 = new ItemDto();
            item_Dto_2.setFood(foods.get(1));
            item_Dto_2.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_3 = new ItemDto();
            item_Dto_3.setFood(foods.get(2));
            item_Dto_3.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));

            dishWithScore.setItem_Dto_1(item_Dto_1);
            dishWithScore.setItem_Dto_2(item_Dto_2);
            dishWithScore.setItem_Dto_3(item_Dto_3);
            dishWithScore.setPunctuation(0.0);
            dishesWithScoreArray.add(dishWithScore);
        }

        return dishesWithScoreArray;
    }


    //Fitness function
    private DishWithScoreThreeItems fitnessOne(DishWithScoreThreeItems dishWithScoreThreeItems, Requirements requirements) {
        DishTotals totals = calculateTotals(dishWithScoreThreeItems);
        Double caloriesPunctuation = Math
                .abs((totals.getTotalCalories() / requirements.getCaloriesburned() * caloriesWeightPercent)
                        - caloriesWeightPercent);

        Double proteinPunctuation = Math
                .abs((totals.getTotalProtein() / requirements.getProtein_req()
                        * proteinWeightPercent)
                        - proteinWeightPercent);

        Double carbsPunctuation = Math.abs(
                (totals.getTotalCarbs() / requirements.getCarbs_req() * carbsWeightPercent)
                        - carbsWeightPercent);

        Double fatPunctuation = Math
                .abs((totals.getTotalFat() / requirements.getFat_req() * fatWeightPercent)
                        - fatWeightPercent);

        Double punctuation = caloriesPunctuation + proteinPunctuation + carbsPunctuation
                + fatPunctuation;
        dishWithScoreThreeItems.setPunctuation(punctuation);

        return dishWithScoreThreeItems;
    }

    private List<DishWithScoreThreeItems> fitnessList(List<DishWithScoreThreeItems> dishesWithScoreArray, Requirements requirements) {
        List<DishWithScoreThreeItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreThreeItems -> {
            finalDishesWithScoreArray.add(fitnessOne(dishWithScoreThreeItems, requirements));
        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreThreeItems> orderByPunctuation(List<DishWithScoreThreeItems> dishesWithScoreArray) {
        List<DishWithScoreThreeItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreThreeItems -> {
            finalDishesWithScoreArray.add(dishWithScoreThreeItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreFourItems, t1) -> {
            return dishWithScoreFourItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreThreeItems> holdOnlyFirst10(List<DishWithScoreThreeItems> dishesWithScoreArray) {
        List<DishWithScoreThreeItems> finalDishesWithScoreArray = new ArrayList<>();
        List<DishWithScoreThreeItems> finalDishesWithScoreArray1 = finalDishesWithScoreArray;
        dishesWithScoreArray.stream().forEach(dishWithScoreThreeItems -> {
            finalDishesWithScoreArray1.add(dishWithScoreThreeItems);
        });
        finalDishesWithScoreArray.sort((dishWithScoreThreeItems, t1) -> {
            return dishWithScoreThreeItems.getPunctuation().compareTo(t1.getPunctuation());
        });
        finalDishesWithScoreArray = finalDishesWithScoreArray.subList(0, 10);
        return finalDishesWithScoreArray;
    }



    private DishTotals calculateTotals(DishWithScoreThreeItems dishWithScoreThreeItems) {
        DishTotals totals = new DishTotals();

        Double item1TotalCalories = ( (dishWithScoreThreeItems.getItem_Dto_1().getFood().getCalories100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_1().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalProteins = ( (dishWithScoreThreeItems.getItem_Dto_1().getFood().getProtein100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_1().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalCarbs = ( (dishWithScoreThreeItems.getItem_Dto_1().getFood().getCarbs100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_1().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalFat = ( (dishWithScoreThreeItems.getItem_Dto_1().getFood().getFat100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_1().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_1().getFood().getDoseg());

        Double item2TotalCalories = ( (dishWithScoreThreeItems.getItem_Dto_2().getFood().getCalories100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_2().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalProteins = ( (dishWithScoreThreeItems.getItem_Dto_2().getFood().getProtein100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_2().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalCarbs = ( (dishWithScoreThreeItems.getItem_Dto_2().getFood().getCarbs100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_2().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalFat = ( (dishWithScoreThreeItems.getItem_Dto_2().getFood().getFat100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_2().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_2().getFood().getDoseg());

        Double item3TotalCalories = ( (dishWithScoreThreeItems.getItem_Dto_3().getFood().getCalories100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_3().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalProteins = ( (dishWithScoreThreeItems.getItem_Dto_3().getFood().getProtein100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_3().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalCarbs = ( (dishWithScoreThreeItems.getItem_Dto_3().getFood().getCarbs100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_3().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalFat = ( (dishWithScoreThreeItems.getItem_Dto_3().getFood().getFat100G()/100) *
                dishWithScoreThreeItems.getItem_Dto_3().getDoseX() *
                dishWithScoreThreeItems.getItem_Dto_3().getFood().getDoseg());


        totals.setTotalCalories(item1TotalCalories + item2TotalCalories + item3TotalCalories);
        totals.setTotalProtein(item1TotalProteins + item2TotalProteins + item3TotalProteins);
        totals.setTotalCarbs(item1TotalCarbs + item2TotalCarbs + item3TotalCarbs);
        totals.setTotalFat(item1TotalFat + item2TotalFat + item3TotalFat);

        return totals;
    }

    private DishWithScoreThreeItems getOnlyFirstDish(List<DishWithScoreThreeItems> dishesWithScoreArray) {
        DishWithScoreThreeItems dishWithScorethreeItems = new DishWithScoreThreeItems();
        dishWithScorethreeItems = dishesWithScoreArray.get(0);
        return dishWithScorethreeItems;
    }

    private List<Double> getBestsDosex(DishWithScoreThreeItems dishesWithScore) {
        List<Double> bestDosex = new ArrayList<>();

        bestDosex.add(dishesWithScore.getItem_Dto_1().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_2().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_3().getDoseX());

        return bestDosex;
    }


    //generate 10 new mixing existing dishes randomly

    private List<DishWithScoreThreeItems> generatecrossedDishes(List<DishWithScoreThreeItems> dishesWithScoreArray) {
        List<DishWithScoreThreeItems> crossedDishes = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            DishWithScoreThreeItems dishWithScoreThreeItems = new DishWithScoreThreeItems();
            ItemDto itemDto1 = new ItemDto();
            ItemDto itemDto2 = new ItemDto();
            ItemDto itemDto3 = new ItemDto();


            itemDto1.setFood(dishesWithScoreArray.get(i).getItem_Dto_1().getFood());
            itemDto2.setFood(dishesWithScoreArray.get(i).getItem_Dto_2().getFood());
            itemDto3.setFood(dishesWithScoreArray.get(i).getItem_Dto_3().getFood());



            itemDto1.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_1().getDoseX());
            itemDto2.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_2().getDoseX());
            itemDto3.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_3().getDoseX());



            dishWithScoreThreeItems.setItem_Dto_1(itemDto1);
            dishWithScoreThreeItems.setItem_Dto_2(itemDto2);
            dishWithScoreThreeItems.setItem_Dto_3(itemDto3);

            crossedDishes.add(dishWithScoreThreeItems);
        }
        return crossedDishes;
    }

    public List<Double> geneticThreeAlgorithm(List<Food> foods, Requirements requirements) {
        DishWithScoreThreeItems finalDish = new DishWithScoreThreeItems();
        List<DishWithScoreThreeItems> population = new ArrayList<>();

        //genetic loop
        for (int i = 0; i < maxIterations; i++) {

            // Initialize population

            List<DishWithScoreThreeItems> finalPopulation1 = population;

            generateOrAddRandomPopulationX4Foods(requirements, foods, population).stream().forEach(dishWithScoreThreeItems -> {
                finalPopulation1.add(dishWithScoreThreeItems);
            });


            //  fitness
            List<DishWithScoreThreeItems> puntuatedPopulation = new ArrayList<>();
            fitnessList(population, requirements).stream().forEach(dishWithScoreThreeItems -> {
                puntuatedPopulation.add(dishWithScoreThreeItems);
            });


            //Order by fitness
            List<DishWithScoreThreeItems> orderedPopulation = new ArrayList<>();
            orderByPunctuation(puntuatedPopulation).stream().forEach(dishWithScoreThreeItems -> {
                orderedPopulation.add(dishWithScoreThreeItems);
            });

            // Select bests
            List<DishWithScoreThreeItems> bests = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation).stream().forEach(dishWithScoreThreeItems -> {
                bests.add(dishWithScoreThreeItems);
            });
            population = bests;


            // Generate crossed dishes
            List<DishWithScoreThreeItems> crossedDishes = new ArrayList<>();
            generatecrossedDishes(bests).stream().forEach(dishWithScoreThreeItems -> {
                crossedDishes.add(dishWithScoreThreeItems);
            });


            // fitness
            List<DishWithScoreThreeItems> puntuatedCrossedDishes = new ArrayList<>();
            fitnessList(crossedDishes, requirements).stream().forEach(dishWithScoreThreeItems -> {
                puntuatedCrossedDishes.add(dishWithScoreThreeItems);
            });


            //add crossed dishes to population
            List<DishWithScoreThreeItems> finalPopulation2 = population;
            puntuatedCrossedDishes.stream().forEach(dishWithScoreThreeItems -> {
                finalPopulation2.add(dishWithScoreThreeItems);
            });

            //Order by fitness
            List<DishWithScoreThreeItems> orderedPopulation2 = new ArrayList<>();
            orderByPunctuation(puntuatedCrossedDishes).stream().forEach(dishWithScoreThreeItems -> {
                orderedPopulation2.add(dishWithScoreThreeItems);
            });

            // Select bests
            List<DishWithScoreThreeItems> bests2 = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation2).stream().forEach(dishWithScoreThreeItems -> {
                bests2.add(dishWithScoreThreeItems);
            });



            //add bests to final population

            for(int j = 0; j < 10; j++){
                population.add(bests2.get(j));
            }
            //Order by fitness
            List<DishWithScoreThreeItems> orderedPopulation3 = new ArrayList<>();
            orderByPunctuation(population).stream().forEach(dishWithScoreThreeItems -> {
                orderedPopulation3.add(dishWithScoreThreeItems);
            });

            population=orderedPopulation3;


            finalDish = getOnlyFirstDish(population);

        }

        return getBestsDosex(finalDish);
    }
}
