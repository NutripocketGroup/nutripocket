package com.nutripocket.nutripocket.utils.genetics;

import com.nutripocket.nutripocket.models.dto.Requirements;
import com.nutripocket.nutripocket.services.implementations.FoodImpl;
import com.nutripocket.nutripocket.utils.genetics.genetics.*;
import com.nutripocket.nutripocket.models.entities.Food;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
@Service
public class GeneticsGlobal {
    @Autowired
    public GeneticOne geneticOne;
    @Autowired
    public GeneticTwo geneticTwo;
    @Autowired
    public GeneticThree geneticThree;
    @Autowired
    public GeneticFour geneticFour;
    @Autowired
    public GeneticFive geneticFive;

    @Autowired
    public FoodImpl foodService;

    public List<Double> geneticGlobalAlgorithm(List<Long> foodsIds, Requirements requirements) {
        List<Food> foods = new ArrayList<>();
        System.out.println("foodsIds: " + foodsIds);
        System.out.println(foodService.getFoodById(1L));
        for (Long foodId : foodsIds) {
            foods.add((foodService.getFoodById(foodId)).get());
        }


        List<Double> dosexList = new ArrayList<Double>();
        int size = foods.size();
        switch (size) {
            case 1:
                dosexList = geneticOne.geneticOneAlgorithm(foods, requirements);
                break;
            case 2:
                dosexList = geneticTwo.geneticTwoAlgorithm(foods, requirements);
                break;
            case 3:
                dosexList = geneticThree.geneticThreeAlgorithm(foods, requirements);
                break;
            case 4:
                dosexList = geneticFour.geneticFourAlgorithm(foods, requirements);
                break;
            case 5:
                dosexList = geneticFive.geneticFiveAlgorithm(foods, requirements);
                break;
            default:
                throw new IllegalArgumentException("Invalid number of items: "+ size);

        }

        return dosexList;
    }
}
