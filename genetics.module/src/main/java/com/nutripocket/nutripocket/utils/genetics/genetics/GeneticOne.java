package com.nutripocket.nutripocket.utils.genetics.genetics;


import com.nutripocket.nutripocket.models.dto.DishWithScoreOneItem;
import com.nutripocket.nutripocket.models.dto.Requirements;
import com.nutripocket.nutripocket.models.entities.Food;
import com.nutripocket.nutripocket.models.dto.ItemDto;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
@Service

public class GeneticOne {
    public List<Double> geneticOneAlgorithm(List<Food> foods, Requirements requirements) {
        DishWithScoreOneItem dishWithScoreOneItem = new DishWithScoreOneItem();
        dishWithScoreOneItem.setItem_Dto_1(new ItemDto());
        dishWithScoreOneItem.getItem_Dto_1().setFood(foods.get(0));
        Food food = dishWithScoreOneItem.item_Dto_1.getFood();


        Double doseX = requirements.getCaloriesburned()/(food.getDoseg()* (food.getCalories100G()/100));
        List<Double> dosexList = new ArrayList<Double>();
        dosexList.add(doseX);

        return dosexList;
    }
}
