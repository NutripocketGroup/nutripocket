package com.nutripocket.nutripocket.utils.genetics.genetics;

import com.nutripocket.nutripocket.models.dto.DishTotals;
import com.nutripocket.nutripocket.models.dto.DishWithScoreFiveItems;
import com.nutripocket.nutripocket.models.dto.Requirements;
import com.nutripocket.nutripocket.models.dto.ItemDto;
import com.nutripocket.nutripocket.models.entities.Food;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service

public class GeneticFive {
    //Parameters of genetic algorithm
    private Double randomLimit;

    private int populationSize = 50;
    private int maxIterations = 50;
    private Double caloriesWeightPercent = 70.0;
    private Double proteinWeightPercent = 20.0;
    private Double fatWeightPercent = 5.0;
    private Double carbsWeightPercent = 5.0;


    private List<DishWithScoreFiveItems> generateOrAddRandomPopulationX5Foods(Requirements requirements, List<Food> foods,
                                                                              List<DishWithScoreFiveItems> existentPopulation) {
        List<DishWithScoreFiveItems> dishesWithScoreArray=new ArrayList<>();
        randomLimit = requirements.getCaloriesburned() / 4;
        if (existentPopulation.size() > 0) {

            List<DishWithScoreFiveItems> finalDishesWithScoreArray = dishesWithScoreArray;
            dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
                finalDishesWithScoreArray.add(dishWithScoreFourItems);
            });
        }

        for (int i = dishesWithScoreArray.size(); i < populationSize; i++) {
            DishWithScoreFiveItems dishWithScore = new DishWithScoreFiveItems();
            ItemDto item_Dto_1 = new ItemDto();
            item_Dto_1.setFood(foods.get(0));
            item_Dto_1.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_2 = new ItemDto();
            item_Dto_2.setFood(foods.get(1));
            item_Dto_2.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_3 = new ItemDto();
            item_Dto_3.setFood(foods.get(2));
            item_Dto_3.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_4 = new ItemDto();
            item_Dto_4.setFood(foods.get(3));
            item_Dto_4.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            ItemDto item_Dto_5 = new ItemDto();
            item_Dto_5.setFood(foods.get(4));
            item_Dto_5.setDoseX((double) Math.round(Math.random() * (randomLimit - 1) + 1));
            dishWithScore.setItem_Dto_1(item_Dto_1);
            dishWithScore.setItem_Dto_2(item_Dto_2);
            dishWithScore.setItem_Dto_3(item_Dto_3);
            dishWithScore.setItem_Dto_4(item_Dto_4);
            dishWithScore.setItem_Dto_5(item_Dto_5);
            dishWithScore.setPunctuation(0.0);
            dishesWithScoreArray.add(dishWithScore);
        }

        return dishesWithScoreArray;
    }


    //Fitness function
    private DishWithScoreFiveItems fitnessOne(DishWithScoreFiveItems dishWithScoreFiveItems, Requirements requirements) {
        DishTotals totals = calculateTotals(dishWithScoreFiveItems);
        Double caloriesPunctuation = Math
                .abs((totals.getTotalCalories() / requirements.getCaloriesburned() * caloriesWeightPercent)
                        - caloriesWeightPercent);

        Double proteinPunctuation = Math
                .abs((totals.getTotalProtein() / requirements.getProtein_req()
                        * proteinWeightPercent)
                        - proteinWeightPercent);

        Double carbsPunctuation = Math.abs(
                (totals.getTotalCarbs() / requirements.getCarbs_req() * carbsWeightPercent)
                        - carbsWeightPercent);

        Double fatPunctuation = Math
                .abs((totals.getTotalFat() / requirements.getFat_req() * fatWeightPercent)
                        - fatWeightPercent);

        Double punctuation = caloriesPunctuation + proteinPunctuation + carbsPunctuation
                + fatPunctuation;
        dishWithScoreFiveItems.setPunctuation(punctuation);

        return dishWithScoreFiveItems;
    }

    private List<DishWithScoreFiveItems> fitnessList(List<DishWithScoreFiveItems> dishesWithScoreArray, Requirements requirements) {
        List<DishWithScoreFiveItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray.add(fitnessOne(dishWithScoreFourItems, requirements));


        });
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreFiveItems> orderByPunctuation(List<DishWithScoreFiveItems> dishesWithScoreArray) {
        List<DishWithScoreFiveItems> finalDishesWithScoreArray = new ArrayList<>();
        dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray.add(dishWithScoreFourItems);
        });

        finalDishesWithScoreArray.sort(Comparator.comparing(DishWithScoreFiveItems::getPunctuation));
        return finalDishesWithScoreArray;
    }

    private List<DishWithScoreFiveItems> holdOnlyFirst10(List<DishWithScoreFiveItems> dishesWithScoreArray) {
        List<DishWithScoreFiveItems> finalDishesWithScoreArray = new ArrayList<>();
        List<DishWithScoreFiveItems> finalDishesWithScoreArray1 = finalDishesWithScoreArray;
        dishesWithScoreArray.stream().forEach(dishWithScoreFourItems -> {
            finalDishesWithScoreArray1.add(dishWithScoreFourItems);
        });
        finalDishesWithScoreArray = finalDishesWithScoreArray.subList(0, 10);
        return finalDishesWithScoreArray;
    }



    private DishTotals calculateTotals(DishWithScoreFiveItems dishWithScoreFiveItems) {
        DishTotals totals = new DishTotals();
        Double item1TotalCalories = ( (dishWithScoreFiveItems.getItem_Dto_1().getFood().getCalories100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalProteins = ( (dishWithScoreFiveItems.getItem_Dto_1().getFood().getProtein100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalCarbs = ( (dishWithScoreFiveItems.getItem_Dto_1().getFood().getCarbs100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_1().getFood().getDoseg());
        Double item1TotalFat = ( (dishWithScoreFiveItems.getItem_Dto_1().getFood().getFat100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_1().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_1().getFood().getDoseg());

        Double item2TotalCalories = ( (dishWithScoreFiveItems.getItem_Dto_2().getFood().getCalories100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalProteins = ( (dishWithScoreFiveItems.getItem_Dto_2().getFood().getProtein100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalCarbs = ( (dishWithScoreFiveItems.getItem_Dto_2().getFood().getCarbs100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_2().getFood().getDoseg());
        Double item2TotalFat = ( (dishWithScoreFiveItems.getItem_Dto_2().getFood().getFat100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_2().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_2().getFood().getDoseg());

        Double item3TotalCalories = ( (dishWithScoreFiveItems.getItem_Dto_3().getFood().getCalories100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalProteins = ( (dishWithScoreFiveItems.getItem_Dto_3().getFood().getProtein100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalCarbs = ( (dishWithScoreFiveItems.getItem_Dto_3().getFood().getCarbs100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_3().getFood().getDoseg());
        Double item3TotalFat = ( (dishWithScoreFiveItems.getItem_Dto_3().getFood().getFat100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_3().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_3().getFood().getDoseg());

        Double item4TotalCalories = ( (dishWithScoreFiveItems.getItem_Dto_4().getFood().getCalories100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalProteins = ( (dishWithScoreFiveItems.getItem_Dto_4().getFood().getProtein100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalCarbs = ( (dishWithScoreFiveItems.getItem_Dto_4().getFood().getCarbs100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_4().getFood().getDoseg());
        Double item4TotalFat = ( (dishWithScoreFiveItems.getItem_Dto_4().getFood().getFat100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_4().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_4().getFood().getDoseg());

        Double item5TotalCalories = ( (dishWithScoreFiveItems.getItem_Dto_5().getFood().getCalories100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_5().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_5().getFood().getDoseg());
        Double item5TotalProteins = ( (dishWithScoreFiveItems.getItem_Dto_5().getFood().getProtein100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_5().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_5().getFood().getDoseg());
        Double item5TotalCarbs = ( (dishWithScoreFiveItems.getItem_Dto_5().getFood().getCarbs100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_5().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_5().getFood().getDoseg());
        Double item5TotalFat = ( (dishWithScoreFiveItems.getItem_Dto_5().getFood().getFat100G()/100) *
                dishWithScoreFiveItems.getItem_Dto_5().getDoseX() *
                dishWithScoreFiveItems.getItem_Dto_5().getFood().getDoseg());



        totals.setTotalCalories(item1TotalCalories + item2TotalCalories + item3TotalCalories + item4TotalCalories + item5TotalCalories);
        totals.setTotalProtein(item1TotalProteins + item2TotalProteins + item3TotalProteins + item4TotalProteins + item5TotalProteins);
        totals.setTotalCarbs(item1TotalCarbs + item2TotalCarbs + item3TotalCarbs + item4TotalCarbs + item5TotalCarbs);
        totals.setTotalFat(item1TotalFat + item2TotalFat + item3TotalFat + item4TotalFat + item5TotalFat);

        return totals;
    }

    private DishWithScoreFiveItems getOnlyFirstDish(List<DishWithScoreFiveItems> dishesWithScoreArray) {
        DishWithScoreFiveItems dishWithScoreFourItems = new DishWithScoreFiveItems();
        dishWithScoreFourItems = dishesWithScoreArray.get(0);
        return dishWithScoreFourItems;
    }

    private List<Double> getBestsDosex(DishWithScoreFiveItems dishesWithScore) {
        List<Double> bestDosex = new ArrayList<>();

        bestDosex.add(dishesWithScore.getItem_Dto_1().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_2().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_3().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_4().getDoseX());
        bestDosex.add(dishesWithScore.getItem_Dto_5().getDoseX());

        return bestDosex;
    }


    //generate 10 new mixing existing dishes randomly

    private List<DishWithScoreFiveItems> generatecrossedDishes(List<DishWithScoreFiveItems> dishesWithScoreArray) {
        List<DishWithScoreFiveItems> crossedDishes = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            DishWithScoreFiveItems dishWithScoreFourItems = new DishWithScoreFiveItems();
            ItemDto itemDto1 = new ItemDto();
            ItemDto itemDto2 = new ItemDto();
            ItemDto itemDto3 = new ItemDto();
            ItemDto itemDto4 = new ItemDto();
            ItemDto itemDto5 = new ItemDto();
            itemDto1.setFood(dishesWithScoreArray.get(i).getItem_Dto_1().getFood());
            itemDto2.setFood(dishesWithScoreArray.get(i).getItem_Dto_2().getFood());
            itemDto3.setFood(dishesWithScoreArray.get(i).getItem_Dto_3().getFood());
            itemDto4.setFood(dishesWithScoreArray.get(i).getItem_Dto_4().getFood());
            itemDto5.setFood(dishesWithScoreArray.get(i).getItem_Dto_5().getFood());

            itemDto1.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_1().getDoseX());
            itemDto2.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_2().getDoseX());
            itemDto3.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_3().getDoseX());
            itemDto4.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_4().getDoseX());
            itemDto5.setDoseX(dishesWithScoreArray.get((int) (Math.random() * 9)).getItem_Dto_5().getDoseX());


            dishWithScoreFourItems.setItem_Dto_1(itemDto1);
            dishWithScoreFourItems.setItem_Dto_2(itemDto2);
            dishWithScoreFourItems.setItem_Dto_3(itemDto3);
            dishWithScoreFourItems.setItem_Dto_4(itemDto4);
            dishWithScoreFourItems.setItem_Dto_5(itemDto5);

            crossedDishes.add(dishWithScoreFourItems);
        }
        return crossedDishes;
    }

    public List<Double> geneticFiveAlgorithm(List<Food> foods, Requirements requirements) {

        DishWithScoreFiveItems finalDish = new DishWithScoreFiveItems();
        List<DishWithScoreFiveItems> population = new ArrayList<>();

        //genetic loop
        for (int i = 0; i < maxIterations; i++) {

            // Initialize population

            List<DishWithScoreFiveItems> finalPopulation1 = population;

            generateOrAddRandomPopulationX5Foods(requirements, foods, population).stream().forEach(dishWithScoreFourItems -> {
                finalPopulation1.add(dishWithScoreFourItems);
            });


            //  fitness
            List<DishWithScoreFiveItems> puntuatedPopulation = new ArrayList<>();
            fitnessList(population, requirements).stream().forEach(dishWithScoreFourItems -> {
                puntuatedPopulation.add(dishWithScoreFourItems);
            });


            //Order by fitness
            List<DishWithScoreFiveItems> orderedPopulation = new ArrayList<>();
            orderByPunctuation(puntuatedPopulation).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation.add(dishWithScoreFourItems);
            });

            // Select bests
            List<DishWithScoreFiveItems> bests = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation).stream().forEach(dishWithScoreFourItems -> {
                bests.add(dishWithScoreFourItems);
            });
            population = bests;


            // Generate crossed dishes
            List<DishWithScoreFiveItems> crossedDishes = new ArrayList<>();
            generatecrossedDishes(bests).stream().forEach(dishWithScoreFourItems -> {
                crossedDishes.add(dishWithScoreFourItems);
            });


            // fitness
            List<DishWithScoreFiveItems> puntuatedCrossedDishes = new ArrayList<>();
            fitnessList(crossedDishes, requirements).stream().forEach(dishWithScoreFourItems -> {
                puntuatedCrossedDishes.add(dishWithScoreFourItems);
            });


            //add crossed dishes to population
            List<DishWithScoreFiveItems> finalPopulation2 = population;
            puntuatedCrossedDishes.stream().forEach(dishWithScoreFourItems -> {
                finalPopulation2.add(dishWithScoreFourItems);
            });

            //Order by fitness
            List<DishWithScoreFiveItems> orderedPopulation2 = new ArrayList<>();
            orderByPunctuation(puntuatedCrossedDishes).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation2.add(dishWithScoreFourItems);
            });

            // Select bests
            List<DishWithScoreFiveItems> bests2 = new ArrayList<>();
            holdOnlyFirst10(orderedPopulation2).stream().forEach(dishWithScoreFourItems -> {
                bests2.add(dishWithScoreFourItems);
            });



       //add bests to final population

            for(int j = 0; j < 10; j++){
                population.add(bests2.get(j));
            }
            //Order by fitness
            List<DishWithScoreFiveItems> orderedPopulation3 = new ArrayList<>();
            orderByPunctuation(population).stream().forEach(dishWithScoreFourItems -> {
                orderedPopulation3.add(dishWithScoreFourItems);
            });

            population=orderedPopulation3;


            finalDish = getOnlyFirstDish(population);

        }

        return getBestsDosex(finalDish);
    }

}
