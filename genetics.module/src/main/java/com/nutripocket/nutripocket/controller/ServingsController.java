package com.nutripocket.nutripocket.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nutripocket.nutripocket.models.dto.GeneticsRequestDto;
import com.nutripocket.nutripocket.utils.genetics.GeneticsGlobal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class ServingsController {

    @Autowired
    private GeneticsGlobal geneticsService;

    @PostMapping(value="/api/genetics",consumes = "application/json")
    public ResponseEntity<?> genetics(@RequestBody String geneticsRequestDtoString) {
        GeneticsRequestDto geneticsRequestDto;
        try{
            ObjectMapper om = new ObjectMapper();
            geneticsRequestDto = om.readValue(geneticsRequestDtoString, GeneticsRequestDto.class);
        } catch (JsonMappingException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        } catch (JsonProcessingException e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
        try {
            return new ResponseEntity<>(geneticsService.geneticGlobalAlgorithm(geneticsRequestDto.getFoods(), geneticsRequestDto.getRequirements()), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
