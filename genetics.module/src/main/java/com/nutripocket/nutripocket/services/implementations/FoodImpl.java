package com.nutripocket.nutripocket.services.implementations;

import com.nutripocket.nutripocket.repositories.FoodDao;
import com.nutripocket.nutripocket.services.interfaces.IFood;
import com.nutripocket.nutripocket.models.entities.Food;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
@Service
public class FoodImpl implements IFood {
    @Autowired
    public FoodDao foodRepository;
    @Override
    public Optional<Food> getFoodById(Long id) {
        return foodRepository.findById(id);
    }

    @Override
    public List<Food> getAllFoods() {
        return null;
    }

    @Override
    public List<Food> getAllValidatedFoods() {
        return null;
    }

    @Override
    public List<Food> getFoodsNonValidated() {
        return null;
    }

    @Override
    public Food addFood(Food food) {
        return null;
    }

    @Override
    public int updateFood(Food food) {
        return 0;
    }

    @Override
    public int deleteFood(double id) {
    return 0;
    }

    @Override
    public List<Food> getFoodsByName(String name) {
        return null;
    }
}
