package com.nutripocket.nutripocket.services.interfaces;

import com.nutripocket.nutripocket.models.entities.Food;

import java.util.List;
import java.util.Optional;

public interface IFood {
    public Optional<Food> getFoodById(Long id);
    public List<Food> getAllFoods();
    public List<Food> getAllValidatedFoods();
    public List<Food> getFoodsNonValidated();
    public Food addFood(Food food);
    public int updateFood(Food food);
    public int deleteFood(double id);
    public List<Food> getFoodsByName(String name);

}
