package com.nutripocket.nutripocket.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.nutripocket.nutripocket.models.entities.Food;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ItemDto {
    Food food;
    Double punctuation;
    Double doseX;

}

