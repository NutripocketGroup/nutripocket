package com.nutripocket.nutripocket.models.dto;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class DishTotals {
    Double totalCalories;
    Double totalProtein;
    Double totalCarbs;
    Double totalFat;
}
