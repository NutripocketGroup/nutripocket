package com.nutripocket.nutripocket.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Requirements {
    Double caloriesburned;
    Double protein_req;
    Double fat_req;
    Double carbs_req;
}
