package com.nutripocket.nutripocket.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishWithScoreFourItems {
    public ItemDto item_Dto_1;
    public ItemDto item_Dto_2;
    public ItemDto item_Dto_3;
    public ItemDto item_Dto_4;
    public Double punctuation=0.0;
}
