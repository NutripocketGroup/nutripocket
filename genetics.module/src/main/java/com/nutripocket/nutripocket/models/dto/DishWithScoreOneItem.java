package com.nutripocket.nutripocket.models.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class DishWithScoreOneItem {
    public ItemDto item_Dto_1;
    public Double punctuation= 0.0;
}
