package com.nutripocket.nutripocket.models.dto;

import lombok.*;

import java.util.List;
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class GeneticsRequestDto {
    private List<Long> foods;
    private Requirements requirements;
}
