package com.nutripocket.nutripocket.models.entities;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
public class Food {
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id")
    private long id;
    @Basic
    @Column(name = "name")
    private String name;
    @Basic
    @Column(name = "nombre")
    private String nombre;
    @Basic
    @Column(name = "calories100g")
    private double calories100G;
    @Basic
    @Column(name = "protein100g")
    private double protein100G;
    @Basic
    @Column(name = "carbs100g")
    private double carbs100G;
    @Basic
    @Column(name = "fat100g")
    private double fat100G;
    @Basic
    @Column(name = "saturatedfat100g")
    private Double saturatedfat100G;
    @Basic
    @Column(name = "sugar100g")
    private Double sugar100G;
    @Basic
    @Column(name = "fiber100g")
    private Double fiber100G;
    @Basic
    @Column(name = "salt100g")
    private Double salt100G;
    @Basic
    @Column(name = "doseg")
    private double doseg;
    @Basic
    @Column(name = "validated")
    private Byte validated;
    @Basic
    @Column(name = "createdAt")
    private Date createdAt;
    @Basic
    @Column(name = "updatedAt")
    private Date updatedAt;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getCalories100G() {
        return calories100G;
    }

    public void setCalories100G(double calories100G) {
        this.calories100G = calories100G;
    }

    public double getProtein100G() {
        return protein100G;
    }

    public void setProtein100G(double protein100G) {
        this.protein100G = protein100G;
    }

    public double getCarbs100G() {
        return carbs100G;
    }

    public void setCarbs100G(double carbs100G) {
        this.carbs100G = carbs100G;
    }

    public double getFat100G() {
        return fat100G;
    }

    public void setFat100G(double fat100G) {
        this.fat100G = fat100G;
    }

    public Double getSaturatedfat100G() {
        return saturatedfat100G;
    }

    public void setSaturatedfat100G(Double saturatedfat100G) {
        this.saturatedfat100G = saturatedfat100G;
    }

    public Double getSugar100G() {
        return sugar100G;
    }

    public void setSugar100G(Double sugar100G) {
        this.sugar100G = sugar100G;
    }

    public Double getFiber100G() {
        return fiber100G;
    }

    public void setFiber100G(Double fiber100G) {
        this.fiber100G = fiber100G;
    }

    public Double getSalt100G() {
        return salt100G;
    }

    public void setSalt100G(Double salt100G) {
        this.salt100G = salt100G;
    }

    public double getDoseg() {
        return doseg;
    }

    public void setDoseg(double doseg) {
        this.doseg = doseg;
    }

    public Byte getValidated() {
        return validated;
    }

    public void setValidated(Byte validated) {
        this.validated = validated;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Food food = (Food) o;
        return id == food.id && Double.compare(food.calories100G, calories100G) == 0 && Double.compare(food.protein100G, protein100G) == 0 && Double.compare(food.carbs100G, carbs100G) == 0 && Double.compare(food.fat100G, fat100G) == 0 && Double.compare(food.doseg, doseg) == 0 && Objects.equals(name, food.name) && Objects.equals(nombre, food.nombre) && Objects.equals(saturatedfat100G, food.saturatedfat100G) && Objects.equals(sugar100G, food.sugar100G) && Objects.equals(fiber100G, food.fiber100G) && Objects.equals(salt100G, food.salt100G) && Objects.equals(validated, food.validated) && Objects.equals(createdAt, food.createdAt) && Objects.equals(updatedAt, food.updatedAt);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, nombre, calories100G, protein100G, carbs100G, fat100G, saturatedfat100G, sugar100G, fiber100G, salt100G, doseg, validated, createdAt, updatedAt);
    }
}
