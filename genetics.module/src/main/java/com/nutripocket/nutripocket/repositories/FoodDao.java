package com.nutripocket.nutripocket.repositories;

import com.nutripocket.nutripocket.models.entities.Food;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface FoodDao extends CrudRepository<Food, Long> {
    public List<Food> getFoodByValidated(boolean validated);

    @Query(value = "SELECT * FROM food WHERE name LIKE %?1% OR nombre LIKE %?1%", nativeQuery = true)
    public List<Food> getFoodsByName(String name);
}
