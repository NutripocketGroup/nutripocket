package com.nutripocket.nutripocket.utils.genetics;

import com.nutripocket.nutripocket.models.dto.Requirements;
import com.nutripocket.nutripocket.models.entities.Food;
import com.nutripocket.nutripocket.repositories.FoodDao;
import com.nutripocket.nutripocket.services.implementations.FoodImpl;
import com.nutripocket.nutripocket.utils.genetics.genetics.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class GeneticsGlobalTest {


    //GeneticGlobal
    GeneticsGlobal geneticsGlobal = new GeneticsGlobal();

    //Five food ids
    List<Long> foods = new ArrayList<>();


    Requirements requirements = new Requirements();

    Food food1 = new Food();
    Food food2 = new Food();
    Food food3 = new Food();
    Food food4 = new Food();
    Food food5 = new Food();


    @Mock
    FoodDao foodDao;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        //Five food ids
        foods.add(1L);
        // foods.add(2L);
       // foods.add(3L);
       // foods.add(4L);
       // foods.add(5L);

        //Five food

        food1.setId(1L);
        food1.setName("Food 1");
        food1.setCalories100G(100.0);
        food1.setDoseg(1.0);
        food1.setProtein100G(10.0);
        food1.setCarbs100G(20.0);
        food1.setFat100G(0.0);




        food2.setId(2L);
        food2.setName("Food 2");
        food2.setCalories100G(200.0);
        food2.setDoseg(2.0);
        food2.setProtein100G(20.0);
        food2.setCarbs100G(40.0);
        food2.setFat100G(5.0);



        food3.setId(3L);
        food3.setName("Food 3");
        food3.setCalories100G(300.0);
        food3.setDoseg(3.0);
        food3.setProtein100G(30.0);
        food3.setCarbs100G(60.0);
        food3.setFat100G(10.0);



        food4.setId(4L);
        food4.setName("Food 4");
        food4.setCalories100G(400.0);
        food4.setDoseg(4.0);
        food4.setProtein100G(40.0);
        food4.setCarbs100G(80.0);
        food4.setFat100G(10.0);



        food5.setId(5L);
        food5.setName("Food 5");
        food5.setCalories100G(500.0);
        food5.setDoseg(5.0);
        food5.setProtein100G(50.0);
        food5.setCarbs100G(100.0);
        food5.setFat100G(10.0);




        //Requirements
        requirements.setCaloriesburned(4000.0);
        requirements.setProtein_req(250.0);
        requirements.setFat_req(50.0);
        requirements.setCarbs_req(300.0);


        //GeneticOne
        geneticsGlobal.geneticOne = new GeneticOne();
        //GeneticTwo
        geneticsGlobal.geneticTwo = new GeneticTwo();
        //GeneticThree
        geneticsGlobal.geneticThree = new GeneticThree();
        //GeneticFour
        geneticsGlobal.geneticFour = new GeneticFour();
        //GeneticFive
        geneticsGlobal.geneticFive = new GeneticFive();
        //Initialize Genetics algorithms food services
        geneticsGlobal.foodService = new FoodImpl();
        //Initialize foods Dao
        geneticsGlobal.foodService.foodRepository = foodDao;



    }

    @Test
    void test_If_Results_List_Size_Is_Equals_EntrySize() {
        when(foodDao.findById(1L)).thenReturn(Optional.of(food1));
        when(foodDao.findById(2L)).thenReturn(Optional.of(food2));
        when(foodDao.findById(3L)).thenReturn(Optional.of(food3));
        when(foodDao.findById(4L)).thenReturn(Optional.of(food4));
        when(foodDao.findById(5L)).thenReturn(Optional.of(food5));
        List<Double> results = geneticsGlobal.geneticGlobalAlgorithm(foods, requirements);
        //print results
for (Double result : results) {
            System.out.println(result);
        }
        System.out.println("Results size: " + results.size());
        System.out.println("Foods size: " + foods.size());
    assertEquals(results.size(), foods.size());
    }






}