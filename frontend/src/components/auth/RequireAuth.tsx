export default function RequireAuth() {
  if (localStorage.getItem("token")) return true;
  return false;
}
