import { FormEvent } from "react";
import styles from "./Login.module.scss";
import blackLogo from "../../assets/black-logo.png";

export default function Login() {
  const handleLogin = (event: FormEvent) => {
    event.preventDefault();
    let userEmail = document.getElementById("user_email") as HTMLInputElement;
    let userPwd = document.getElementById("user_pwd") as HTMLInputElement;

    console.log(userEmail.value, userPwd.value);
  };

  return (
    <div className={styles.login_main}>
      <div className={styles.logo}>
        <img src={blackLogo} alt="Nutri Logo" />
        <p>Dynamic diets only for you.</p>
      </div>
      <p className={styles.nutri_phone}>**Phone Logo Goes Here**</p>
      <div className={styles.user_login}>
        <form onSubmit={handleLogin}>
          <label htmlFor="user_email">Email</label>
          <input type="email" placeholder="Email" id="user_email" />
          <label htmlFor="user_pwd">Password</label>
          <input type="password" placeholder="Password" id="user_pwd" />
          <button type="submit">Login</button>
        </form>
      </div>
    </div>
  );
}
