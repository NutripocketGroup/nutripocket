import express from "express";

import passport from "passport";
import morgan from "morgan";
import cors from "cors";

import foodRoutes from "./api/routes/food.routes";

import db from "./db/config";

//Declare app express server
const app = express();
//Swagger?

//Initialize database
db.sequelize.sync({ force: false });

app.use(express.json());

app.use(cors());

//routes
app.use("/api/food", foodRoutes);

const PORT = process.env.PORT || 3002;

app.use(morgan("dev")); //tiny
app.use(passport.initialize());
try {
  app.listen(PORT, () => {
    console.log(`Server running on port ${PORT}`);
  });
} catch (error: any) {
  console.error(`Error ocurred: ${error.message}`);
}
