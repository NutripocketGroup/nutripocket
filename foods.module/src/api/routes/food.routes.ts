import express from "express";
import { FoodController } from "../controllers/food.controller";

const router = express.Router();
const foodController =  new FoodController();


router.post("/", foodController.createUserFood);//
router.post("/admin/", foodController.createAdminFood);
router.get("/", foodController.getAllFood);//
router.get("/validated", foodController.getValidatedFood);//
router.get("/nonvalidated", foodController.getNonValidatedFood);//
router.get("/:id", foodController.getFoodById);//
router.get("/byname/:name", foodController.getFoodByName);//
router.put("/:id", foodController.updateFood);//
router.delete("/:id", foodController.deleteFood);//
router.get("/admin/validate/:id", foodController.validateFood);//
router.get("/admin/unvalidate/:id", foodController.unvalidateFood);//

export default router;
