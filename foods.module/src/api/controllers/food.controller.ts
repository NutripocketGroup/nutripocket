import { Food } from "./../../db/models/food.model";
import { Request, Response } from "express";
import { FoodService } from "../services/food.service";

export class FoodController {
    foodService = FoodService.getInstance();

    createUserFood = (req: Request, res: Response) => {
        if (
            !req.body.name ||
            !req.body.nombre ||
            !req.body.calories100g ||
            !req.body.protein100g ||
            !req.body.carbs100g ||
            !req.body.fat100g ||
            !req.body.doseg
        ) {
            res.status(400).json({
                message: "Content cannot be empty",
            });
            return;
        }
        const food = new Food();
        food.name = req.body.name;
        food.nombre = req.body.nombre;
        food.calories100g = req.body.calories100g;
        food.protein100g = req.body.protein100g;
        food.carbs100g = req.body.carbs100g;
        food.fat100g = req.body.fat100g;
        food.doseg = req.body.doseg;
        food.saturatedfat100g = req.body.saturatedfat100g;
        food.sugar100g = req.body.sugar100g;
        food.fiber100g = req.body.fiber100g;
        food.salt100g = req.body.salt100g;

        this.foodService
            .createUserFood(food)
            .then((food) => {
                res.status(201).json({
                    message: "Food created successfully",
                    food,
                });
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error creating food",
                    error,
                });
            });
    };

    createAdminFood = (req: Request, res: Response) => {
        if (
            !req.body.name ||
            !req.body.nombre ||
            !req.body.calories100g ||
            !req.body.protein100g ||
            !req.body.carbs100g ||
            !req.body.fat100g ||
            !req.body.doseg
        ) {
            res.status(400).json({
                message: "Content cannot be empty",
            });
            return;
        }
        const food = new Food();
        food.name = req.body.name;
        food.nombre = req.body.nombre;
        food.calories100g = req.body.calories100g;
        food.protein100g = req.body.protein100g;
        food.carbs100g = req.body.carbs100g;
        food.fat100g = req.body.fat100g;
        food.doseg = req.body.doseg;
        food.saturatedfat100g = req.body.saturatedfat100g;
        food.sugar100g = req.body.sugar100g;
        food.fiber100g = req.body.fiber100g;
        food.salt100g = req.body.salt100g;

        this.foodService
            .createAdminFood(food)
            .then((food) => {
                res.status(201).json({
                    message: "Food created successfully",
                    food,
                });
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error creating food",
                    error,
                });
            });
    };

    getAllFood = (_req: Request, res: Response) => {
        this.foodService
            .findAllFoods()
            .then((foods) => {
                return res.status(200).json({
                    message: "Foods retrieved successfully",
                    foods,
                });
            })
            .catch((error) => {
                return res.status(500).json({
                    message: "Error retrieving foods",
                    error,
                });
            });
    };

    getValidatedFood = (_req: Request, res: Response) => {
        this.foodService
            .findValidatedFoods()
            .then((foods) => {
                return res.status(200).json({
                    message: "Foods retrieved successfully",
                    foods,
                });
            })
            .catch((error) => {
                return res.status(500).json({
                    message: "Error retrieving foods",
                    error,
                });
            });
    };


    getNonValidatedFood = (_req: Request, res: Response) => {
        this.foodService
            .findNonValidatedFoods()
            .then((foods) => {
                return res.status(200).json({
                    message: "Foods retrieved successfully",
                    foods,
                });
            })
            .catch((error) => {
                return res.status(500).json({
                    message: "Error retrieving foods",
                    error,
                });
            });
    };

    getFoodById = (req: Request, res: Response) => {
        const id: number = parseInt(req.params.id);
        if (!id) {
            res.status(400).json({
                message: "Id is required",
            });
            return;
        }

        this.foodService
            .findFoodById(id)
            .then((food) => {
                res.status(200).json({
                    message: "Food retrieved successfully",
                    food,
                });
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error retrieving food",
                    error,
                });
            });
    };

    getFoodByName = (req: Request, res: Response) => {
        const name: string = req.params.name;
        if (!name) {
            res.status(400).json({
                message: "Name is required",
            });
            return;
        }

        this.foodService
            .findFoodsByNameOrNombre(name)
            .then((foods) => {
                res.status(200).json({
                    message: "Food retrieved successfully",
                    foods,
                });
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error retrieving food",
                    error,
                });
            });
    };

    updateFood = (req: Request, res: Response) => {
        const id: number = parseInt(req.params.id);
        const food: Food = req.body;
        if (!id || !food) {
            res.status(400).json({
                message: "Id is required",
            });
            return;
        }

        const createdFood = new Food();
        createdFood.id = id;
        createdFood.name = food.name;
        createdFood.nombre = food.nombre;
        createdFood.calories100g = food.calories100g;
        createdFood.protein100g = food.protein100g;
        createdFood.carbs100g = food.carbs100g;
        createdFood.fat100g = food.fat100g;
        createdFood.doseg = food.doseg;
        createdFood.saturatedfat100g = food.saturatedfat100g;
        createdFood.sugar100g = food.sugar100g;
        createdFood.fiber100g = food.fiber100g;
        createdFood.salt100g = food.salt100g;

        this.foodService
            .updateFood(createdFood)
            .then((count) => {
                if (count[0] == 1) {
                    res.status(200).json({
                        message: "Food updated successfully",
                    });
                }
                else {
                    res.status(404).json({
                        message: "Food not found",
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error updating food",
                    error,
                });
            });
    };

    deleteFood = (req: Request, res: Response) => {
        const id: number = parseInt(req.params.id);
        if (!id) {
            res.status(400).json({
                message: "Id is required",
            });
            return;
        }

        this.foodService
            .deleteFood(id)
            .then((num) => {
                if (num == 1) {
                    res.status(200).json({
                        message: "Food deleted successfully",
                    }
                    );
                } else {
                    res.status(404).json({
                        message: "Food not found",
                    }
                    );
                }

            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error deleting food",
                    error,
                });
            });
    };

    validateFood = (req: Request, res: Response) => {
        const id: number = parseInt(req.params.id);
        const food: Food = new Food();
        if (!id) {
            res.status(400).json({
                message: "Id is required",
            });
            return;
        }

        food.id = id;
        food.validated = true;

        this.foodService
            .updateFood(food)
            .then((count) => {
                if (count[0] == 1) {
                    res.status(200).json({
                        message: "Food validated successfully",
                        count: count[0],
                    });
                }
                else {
                    res.status(404).json({
                        message: "Food not found or already validated",
                        count: count[0],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error updating food",
                    error,
                });
            });
    };

    unvalidateFood = (req: Request, res: Response) => {
        const id: number = parseInt(req.params.id);
        const food: Food = new Food();
        if (!id) {
            res.status(400).json({
                message: "Id is required",
                
            });
            return;
        }

        food.id = id;
        food.validated = false;

        this.foodService
            .updateFood(food)
            .then((count) => {
                if (count[0] == 1) {
                    res.status(200).json({
                        message: "Food unvalidated successfully",
                        count: count[0],
                    });
                }
                else {
                    res.status(404).json({
                        message: "Food not found or already unvalidated",
                        count: count[0],
                    });
                }
            })
            .catch((error) => {
                res.status(500).json({
                    message: "Error updating food",
                    error,
                });
            });
    };
}
