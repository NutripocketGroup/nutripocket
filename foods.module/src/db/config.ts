
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../config/config")[env];

import  FoodModel  from "./models/food.model";
import { Dialect, Sequelize } from "sequelize";


const database = config.database as string;
const username = config.username as string;
const host = config.host;
const dialect = config.dialect as Dialect;
const password = config.password;
const port = config.port;

const sequelize = new Sequelize(database, username, password, {
  host: host,
  dialect: dialect,
  port: port,
  benchmark: true,
});
sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}
).catch(err => {
  console.error("Unable to connect to the database:", err);
}
);


const db = {
  Food: FoodModel(sequelize), 
  sequelize, // connection instance (RAW queries)
  Sequelize, // library
};

//Initialize associations
/* setAssociations(db);
 */


export default db;
