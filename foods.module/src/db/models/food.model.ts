import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class Food {
  id?: number;
  name: string;
  nombre: string;
  calories100g: number;
  protein100g: number;
  carbs100g: number;
  fat100g: number;
  saturatedfat100g?: number;
  sugar100g?: number;
  fiber100g?: number;
  salt100g?: number;
  doseg: number;
  validated?: boolean;
}

export type FoodCreationAttributes = Optional<
  Food,
  | "id"
  | "name"
  | "nombre"
  | "calories100g"
  | "protein100g"
  | "carbs100g"
  | "fat100g"
  | "doseg"
>;

export class FoodModel
  extends Model<Food, FoodCreationAttributes>
  implements Food
{
  public id?: number;
  public name: string;
  public nombre: string;
  public calories100g: number;
  public protein100g: number;
  public carbs100g: number;
  public fat100g: number;
  public saturatedfat100g?: number;
  public sugar100g?: number;
  public fiber100g?: number;
  public salt100g?: number;
  public doseg: number;
  public validated?: boolean;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

export default function (sequelize: Sequelize): typeof FoodModel {
  FoodModel.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
      nombre: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      calories100g: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      protein100g: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      carbs100g: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      fat100g: {
        type: DataTypes.FLOAT,
        allowNull: false,
      },
      saturatedfat100g: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      sugar100g: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      fiber100g: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      salt100g: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      doseg: {
        type: DataTypes.FLOAT,
        allowNull: false,
        defaultValue: 1,
      },
      validated: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: false,
      }
    },
    {
      sequelize,
      tableName: "food",
      timestamps: true,
    } 
  );
  return FoodModel;
}
