
import { Request, Response, NextFunction } from 'express';


export function scopesValidationHandler() {
  return function (req: Request, res: Response, next: NextFunction) {

    let method = req.method;
    let api = req.url.split('/')[2];  //get api from url
    const allowedScopes = setPermissions(method, api, req); //generate list of allowed scopes depending on method and api

    const authInfo: any = req["authInfo"];
    const scopes: string[] = authInfo.scopes;
    console.log(scopes);

    if (!scopes) {
      next(res.status(401).json({ message: "Missing scopes" }));
    }

    const hasAccess = allowedScopes.some(scope => scopes.includes(scope));
    console.log(allowedScopes.some(scope => scopes.includes(scope)))
    if (hasAccess) {
      next();
    } else {
      next(res.status(401).json({ message: "Unauthorized" }));
    }
  };



  function setPermissions(method, api, req){
    let allowedScopes = [];
    //User Permissions
    if (api === 'user') {
      if (method === 'GET') {
        allowedScopes.push('read:food');
      }
      if (method === 'POST') {
        allowedScopes.push('create:food');
      }
      if (method === 'PUT') {
        allowedScopes.push('update:food');
      }
      if (method === 'DELETE') {
        allowedScopes.push('delete:food');
      }
      if (req.url.split('/').includes("admin")) {
        allowedScopes.push('admin:food');
      }
    }
    //Food Permissions
    if (api === 'food') {
      if (method === 'GET') {
        allowedScopes.push('read:food');
      }
      if (method === 'POST') {
        allowedScopes.push('create:food');
      }
      if (method === 'PUT') {
        allowedScopes.push('update:food');
      }
      if (method === 'DELETE') {
        allowedScopes.push('delete:food');
      }
      if (req.url.split('/').includes("admin")) {
        allowedScopes.push('admin:food');
      }
    }
    //Diet Permissions
    if (api === 'diet') {
      if (method === 'GET') {
        allowedScopes.push('read:diet');
      }
      if (method === 'POST') {
        allowedScopes.push('create:diet');
      }
      if (method === 'PUT') {
        allowedScopes.push('update:diet');
      }
      if (method === 'DELETE') {
        allowedScopes.push('delete:diet');
      }
      if (req.url.split('/').includes('admin')) {
        allowedScopes.push('admin:diet');
      }
    }
    return allowedScopes;
  }



}

