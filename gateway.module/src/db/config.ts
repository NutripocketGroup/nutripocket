/* const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../config/config")[env];
import UserModel from "./models/user.model";
import UserScopesModel from "./models/userscopes.model";

import { Dialect, Sequelize } from "sequelize";
import  ScopesModel  from "./models/scopes.model";
import { setAssociations } from "./associations";

const database = config.database as string;
const username = config.username as string;
const host = config.host;
const dialect = config.dialect as Dialect;
const password = config.password;
const port = config.port;

const sequelize = new Sequelize(database, username, password, {
  host: host,
  dialect: dialect,
  port: port,
  benchmark: true,
});
sequelize.authenticate();

const db = {
  User: UserModel(sequelize),
  Scopes: ScopesModel(sequelize),
  UserScopes: UserScopesModel(sequelize),
  sequelize, // connection instance (RAW queries)
  Sequelize, // library
};

//Initialize associations
setAssociations(db);



export default db;
 */