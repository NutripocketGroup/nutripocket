/* 
import {
  Sequelize,
  DataTypes,
  Model,
  Optional,
} from "sequelize";


export interface Scopes {
  id: number;
  name: string;
  role: string;
}

export type ScopesCreationAttributes = Optional<Scopes, "id" | "name" | "role">;

export class ScopesModel
  extends Model<Scopes, ScopesCreationAttributes>
  implements Scopes
{
  public id: number;
  public name: string;
  public role: string;

}

export default function (sequelize: Sequelize): typeof ScopesModel {
  ScopesModel.init(
    {
      id: {
        autoIncrement: true,
        type: DataTypes.BIGINT,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(50),
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING(10),
        allowNull: false,
      },
    },
    {
      sequelize,
      tableName: "scopes",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
      ],
    }
  );
  return ScopesModel;
}
 */