import express from "express";
import { AuthController } from "../controllers/auth.controller";


const router = express.Router();
const authController = new AuthController();



router.all(
  "/auth/*",

  authController.authHandle
);



export default router;