import { RequestController } from './../controllers/request.controller';
import express from "express";



//JWT Strategy
import "../../utils/auth/strategies/jwt";
/* import passport from 'passport';
import { scopesValidationHandler } from '../../middlewares/scopesValidationHandler'; */

const router = express.Router();
const requestController = new RequestController();



router.all(
  "/api/*",
  requestController.requestHandle
);


export default router;