
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../../config/config")[env];
import axios from 'axios';

import { Request, Response } from 'express';


export class AuthController {


    // Create and Save a new user
    authHandle =  (req: Request, res: Response) => {
        let headers = req.headers;
        let body = req.body || {};
        let method = req.method;
        let url = req.url;
        let api=config.usersApi;
        let headersInit: HeadersInit = {
            'Content-Type': headers['content-type'] || 'application/json',
            'Accept': headers['accept'] || 'application/json',
            'Authorization': headers['authorization'] || '',
        };

        //Set api url

        const axiosOptions={
            method: method,
            url: api + url,
            headers: headersInit,
            data: body
        }

        //Redirect request to api depending on method

        axios(axiosOptions).then(response => {
            res.status(response.status).json(response.data);
        }).catch(error => {
            res.status(error.response.code).json({ message: error.message, error: error });
        });
    };

}
