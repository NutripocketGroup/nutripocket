
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../../config/config")[env];
import axios from 'axios';

import { Request, Response } from 'express';




export class RequestController {

    requestHandle =  (req: Request, res: Response) => {
        let headers = req.headers;
        let body = req.body || {};
        let method = req.method;
        let url = req.url;
        let api;
        let headersInit: HeadersInit = {
            'Content-Type': headers['content-type'] || 'application/json',
            'Accept': headers['accept'] || 'application/json',
            'Authorization': headers['authorization'] || '',
        };

        //Set api url

        if (url.split('/')[2] === 'user') {
            api = config.usersApi;
        }
        else if (url.split('/')[2] === 'food') {
            api = config.foodsApi;
        }
        else if (url.split('/')[2] === 'diet') {
            api = config.dietsApi;
        }
        /* else if (url.split('/')[2] === 'genetic') {
            api = config.geneticsApi;
        } */
        else {
            res.status(404).send('Not found');
        }

        const axiosOptions={
            method: method,
            url: api + url,
            headers: headersInit,
            data: body
        }

        //Redirect request to api depending on method

        axios(axiosOptions).then(response => {
            
            res.status(response.status).json(response.data);
        }).catch(error => {
            res.status(error.status! | 500).json({ message: error.message, error: error });
        });
    };
}
