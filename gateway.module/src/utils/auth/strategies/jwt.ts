import passport from "passport";
import boom from "@hapi/boom";
import { Strategy, ExtractJwt } from "passport-jwt";

const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../../../config/config")[env];

passport.use(
  new Strategy(
    {
      secretOrKey: config.jwtSecret,
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    },
    async function (tokenPayload, cb) {
    
      try {
        cb(null, tokenPayload.email, { scopes: tokenPayload.scopes });
      } catch (error: any) {
        return cb(boom.unauthorized("Something went wrong " + error));
      }
    }
  )
);
