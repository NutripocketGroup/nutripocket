
/* /home/rubendev / Escritorio / Projects / NUTRIPOCKET / nutripocket / backend.package / users.module /.env */
import { config } from 'dotenv';


if (process.env.NODE_ENV !== 'production') {
  config({ path: './.development.env' });
}
else {
  config();
}


module.exports = {
  development: {
    usersApi:process.env.USERS_API!,
    foodsApi:process.env.FOODS_API!,
    dietsApi:process.env.DIETS_API!,
    geneticsApi:process.env.GENETICS_API!,
  
    jwtSecret: process.env.JWT_SECRET!
  },
  test: {
    usersApi:process.env.USERS_API!,
    foodsApi:process.env.FOODS_API!,
    dietsApi:process.env.DIETS_API!,
    geneticsApi:process.env.GENETICS_API!,

    jwtSecret: process.env.JWT_SECRET!
  },
  production: {
    usersApi:process.env.USERS_API!,
    foodsApi:process.env.FOODS_API!,
    dietsApi:process.env.DIETS_API!,
    geneticsApi:process.env.GENETICS_API!,

    jwtSecret: process.env.JWT_SECRET!
  }
} 
