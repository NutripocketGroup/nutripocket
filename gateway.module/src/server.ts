import express from "express";

import passport from "passport";
import morgan from "morgan";
import cors from "cors";

import requestHandlerRoutes from "./api/routes/request.routes";
import authRoutes from "./api/routes/auth.routes";





//Declare app express server
const app = express();
//Swagger?




app.use(express.json());

app.use(cors());

//routes
app.use("/", requestHandlerRoutes);
app.use("/", authRoutes);
/*app.use("/api/scope", scopeRoutes); */


const PORT = process.env.PORT || 3000;

app.use(morgan("dev")); //tiny
app.use(passport.initialize());
try {
  app.listen(PORT, () => {
    console.log(`API Gateway running on port ${PORT}`);
  });
} catch (error: any) {
  console.error(`Error ocurred: ${error.message}`);
}
