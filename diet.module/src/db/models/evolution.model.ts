import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export interface Evolution {
  id: number;
  useremail: string;
  caloriesburned?: number;
  calreq: number;
  proteinreq?: number;
  carbreq?: number;
  fatreq?: number;
  weight: number;
  sportshoursweek: number;
  date: string;
}


export type EvolutionCreationAttributes = Optional<Evolution, "id" | "useremail" | "caloriesburned" | "calreq" | "proteinreq" | "carbreq" | "fatreq" | "weight" | "sportshoursweek" | "date">;

export class EvolutionModel extends Model<Evolution, EvolutionCreationAttributes> implements Evolution {
  id!: number;
  useremail!: string;
  caloriesburned?: number;
  calreq!: number;
  proteinreq?: number;
  carbreq?: number;
  fatreq?: number;
  weight!: number;
  sportshoursweek!: number;
  date!: string;
  public readonly createdAt!: string;
  public readonly updatedAt!: string;
}

export default function (sequelize: Sequelize): typeof EvolutionModel {
  EvolutionModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    useremail: {
      type: DataTypes.STRING(500),
      allowNull: false,
      references: {
        model: 'user',
        key: 'email'
      }
    },
    caloriesburned: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    calreq: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    proteinreq: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    carbreq: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    fatreq: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    weight: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    sportshoursweek: {
      type: DataTypes.INTEGER,
      allowNull: false
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'evolution',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_evolution_user",
        using: "BTREE",
        fields: [
          { name: "useremail" },
        ]
      },
    ]
  });
  return EvolutionModel;
}
