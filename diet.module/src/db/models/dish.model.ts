import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class Dish {
  id?: number;
  dayid: number;
}

export type DishCreationAttributes = Optional<Dish, "id" | "dayid">;

  export class DishModel extends Model<Dish, DishCreationAttributes> implements Dish {


  id!: number;
  dayid!: number;
  public readonly createdAt!: string;
  public readonly updatedAt!: string;

    }


export default function (sequelize: Sequelize): typeof DishModel {
  DishModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    dayid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'day',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'dish',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_dish_day",
        using: "BTREE",
        fields: [
          { name: "dayid" },
        ]
      },
    ]
  });
  return DishModel;
}
