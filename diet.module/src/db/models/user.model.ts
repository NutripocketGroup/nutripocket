import { Sequelize, DataTypes, Model, Optional } from "sequelize";


export interface User {
  email: string;
  password: string;
  name: string;
  surname: string;
  role: string;
  birth: Date;
  genre: string;
  heightcm: number;
  startweight: number;
  goalweight: number;
  typeofdiet: number;
  createdAt?: Date;
  updatedAt?: Date;

}

export type UserCreationAttributes = Optional<
  User,
  "email" | "password" | "name"
>;

export class UserModel
  extends Model<User, UserCreationAttributes>
  implements User
{
  public email: string;
  public password: string;
  public name: string;
  public surname: string;
  public role: string;
  public birth: Date;
  public genre: string;
  public heightcm: number;
  public startweight: number;
  public goalweight: number;
  public typeofdiet: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  //Associations
  /* public getUserScopes: HasManyGetAssociationsMixin<UserScopesModel>; */
  
}

export default function (sequelize: Sequelize): typeof UserModel {
  UserModel.init(
    {
      email: {
        type: DataTypes.STRING(500),
        allowNull: false,
        primaryKey: true,
      },
      password: {
        type: DataTypes.STRING(500),
        allowNull: true,
      },
      name: {
        type: DataTypes.STRING(500),
        allowNull: false,
      },
      surname: {
        type: DataTypes.STRING(1000),
        allowNull: true,
      },
      birth: {
        type: DataTypes.DATEONLY,
        allowNull: true,
      },
      genre: {
        type: DataTypes.STRING(10),
        allowNull: true,
      },
      heightcm: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      startweight: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      goalweight: {
        type: DataTypes.FLOAT,
        allowNull: true,
      },
      typeofdiet: {
        type: DataTypes.BIGINT,
        allowNull: true,
        references: {
          model: {
            tableName: "recipe_type",
          },
          key: "id",
        },
      },
      role: {
        type: DataTypes.STRING(50),
        allowNull: false,
        defaultValue: "user",
      },
    },
    {
      sequelize,
      tableName: "user",
      timestamps: true,
    }
  );
  return UserModel;
}
