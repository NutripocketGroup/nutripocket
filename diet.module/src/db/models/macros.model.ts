import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class Macros {
  id?: number;
  recipetype: number;
  proteinperkg?: number;
  fatperkg?: number;
}

export type macrosCreationAttributes = Optional<Macros, "id" | "recipetype">;

  export class MacrosModel extends Model<Macros, macrosCreationAttributes> implements Macros {


  id!: number;
  recipetype!: number;
  proteinperkg?: number;
  fatperkg?: number;
  }

  export default function (sequelize: Sequelize): typeof MacrosModel {
    MacrosModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    recipetype: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'recipe_type',
        key: 'id'
      }
    },
    proteinperkg: {
      type: DataTypes.FLOAT,
      allowNull: true
    },
    fatperkg: {
      type: DataTypes.FLOAT,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'macros',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_macros_recipeType",
        using: "BTREE",
        fields: [
          { name: "recipetype" },
        ]
      },
    ]
  });
  return MacrosModel;
}
