import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class Item {
  id?: number;
  dosex: number;
  dishid: number;
  foodid?: number;
}

export type ItemCreationAttributes = Optional<Item, "id" | "dosex" | "dishid" | "foodid">;

  export class ItemModel extends Model<Item, ItemCreationAttributes> implements Item { 
  id!: number;
  dosex!: number;
  dishid!: number;
  foodid?: number;
  public readonly createdAt!: string;
  public readonly updatedAt!: string;
  }

  export default function (sequelize: Sequelize): typeof ItemModel {
    ItemModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    dosex: {
      type: DataTypes.FLOAT,
      allowNull: false
    },
    dishid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      defaultValue: 1,
      references: {
        model: 'dish',
        key: 'id'
      }
    },
    foodid: {
      type: DataTypes.BIGINT,
      allowNull: true,
      references: {
        model: 'food',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'item',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_dish_item",
        using: "BTREE",
        fields: [
          { name: "dishid" },
        ]
      },
      {
        name: "FK_food_item",
        using: "BTREE",
        fields: [
          { name: "foodid" },
        ]
      },
    ]
  });
  return ItemModel;
}
