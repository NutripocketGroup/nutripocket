import { Sequelize, DataTypes, Model, Optional } from "sequelize";


export class Day {
  id?: number;
  date?: string;
  useremail: string;
  daycaloriesburn?: number;
  daycalorieseaten?: number;
}

export type DayCreationAttributes = Optional<Day, "id" | "date" | "useremail" >

export class DayModel extends Model<Day, DayCreationAttributes> implements Day {
  id!: number;
  date?: string;
  useremail!: string;
  daycaloriesburn?: number;
  daycalorieseaten?: number;
}

export default function (sequelize: Sequelize): typeof DayModel {
  DayModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    date: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    useremail: {
      type: DataTypes.STRING(500),
      allowNull: false,
      references: {
        model: 'user',
        key: 'email'
      }
    },
    daycaloriesburn: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    },
    daycalorieseaten: {
      type: DataTypes.FLOAT,
      allowNull: true,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'day',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_day_email",
        using: "BTREE",
        fields: [
          { name: "useremail" },
        ]
      },
    ]
  });
  return DayModel;
}
