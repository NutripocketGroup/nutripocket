import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export interface DayFood {
  id?: number;
  name: string;
  nombre: string;
}

export type DayFoodCreationAttributes = Optional<DayFood, "id" | "name" | "nombre">;

export class DayFoodModel extends Model<DayFood, DayFoodCreationAttributes> implements DayFood {

  id!: number;
  name!: string;
  nombre!: string;
}

export default function (sequelize: Sequelize): typeof DayFoodModel {
  DayFoodModel.init({

    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    },
    nombre: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'dayfood',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return DayFoodModel;
}
