import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class RecipeFood {
  id?: number;
  foodid: number;
  recipeid: number;
}

export type RecipeFoodCreationAttributes = Optional<
  RecipeFood,
  "id" | "foodid" | "recipeid"
>;

  export class RecipeFoodModel extends Model<RecipeFood, RecipeFoodCreationAttributes> implements RecipeFood {

  id!: number;
  foodid!: number;
  recipeid!: number;

  }

  export default function (sequelize: Sequelize): typeof RecipeFoodModel {
    RecipeFoodModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    foodid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'food',
        key: 'id'
      }
    },
    recipeid: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'recipe',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'recipe_food',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_food_recipefood",
        using: "BTREE",
        fields: [
          { name: "foodid" },
        ]
      },
      {
        name: "FK_recipe_recipefood",
        using: "BTREE",
        fields: [
          { name: "recipeid" },
        ]
      },
    ]
  });
  return RecipeFoodModel;
}
