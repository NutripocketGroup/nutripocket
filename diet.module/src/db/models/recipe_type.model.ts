import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class RecipeType {
  id?: number;
  name: string;
}



export type RecipeTypeCreationAttributes = Optional<
  RecipeType,
  | "id"
  | "name"
>;

export class RecipeTypeModel extends Model<RecipeType, RecipeTypeCreationAttributes> implements RecipeType {
  name!: string;
}

  export default function (sequelize: Sequelize): typeof RecipeTypeModel {
    RecipeTypeModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    name: {
      type: DataTypes.STRING(200),
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'recipe_type',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
  return RecipeTypeModel;
}
