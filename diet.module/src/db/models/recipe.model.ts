import { Sequelize, DataTypes, Model, Optional } from "sequelize";

export class Recipe {
  id?: number;
  type: number;
  foodofday: number;

}

export type RecipeCreationAttributes = Optional< Recipe, "id" | "type" | "foodofday" >;

  export class RecipeModel extends Model<Recipe, RecipeCreationAttributes> implements Recipe {
  id!: number;
  type!: number;
  foodofday!: number;
  public readonly createdAt!: string;
  public readonly updatedAt!: string;
 }

  export default function (sequelize: Sequelize): typeof RecipeModel {
    RecipeModel.init({
    id: {
      autoIncrement: true,
      type: DataTypes.BIGINT,
      allowNull: false,
      primaryKey: true
    },
    type: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'recipe_type',
        key: 'id'
      }
    },
    foodofday: {
      type: DataTypes.BIGINT,
      allowNull: false,
      references: {
        model: 'dayfood',
        key: 'id'
      }
    }
  }, {
    sequelize,
    tableName: 'recipe',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_recipe_recypetype",
        using: "BTREE",
        fields: [
          { name: "type" },
        ]
      },
      {
        name: "FK_recipe_dayFood",
        using: "BTREE",
        fields: [
          { name: "foodofday" },
        ]
      },
    ]
  });
  return RecipeModel;
}
