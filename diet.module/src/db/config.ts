
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "./../config/config")[env];


import { Dialect, Sequelize } from "sequelize";
import { setAssociations } from "./associations";
import DayModel from "./models/day.model";
import DayFoodModel from "./models/dayfood.model";
import DishModel from "./models/dish.model";
import EvolutionModel from './models/evolution.model';
import FoodModel from "./models/food.model";
import ItemModel from "./models/item.model";
import MacrosModel from "./models/macros.model";
import RecipeModel from "./models/recipe.model";
import RecipeFood from "./models/recipe_food.model";
import RecipeTypeModel from "./models/recipe_type.model";


const database = config.database as string;
const username = config.username as string;
const host = config.host;
const dialect = config.dialect as Dialect;
const password = config.password;
const port = config.port;

const sequelize = new Sequelize(database, username, password, {
  host: host,
  dialect: dialect,
  port: port,
  benchmark: true,
});
sequelize.authenticate().then(() => {
  console.log("Connection has been established successfully.");
}
).catch(err => {
  console.error("Unable to connect to the database:", err);
}
);


const db = {
  Day: DayModel(sequelize),
  DayFood: DayFoodModel(sequelize),
  Dish: DishModel(sequelize),
  Evolution: EvolutionModel(sequelize),
  Food: FoodModel(sequelize),
  Item: ItemModel(sequelize),
  RecipeFood: RecipeFood(sequelize),
  RecipeType: RecipeTypeModel(sequelize),
  Recipe: RecipeModel(sequelize),
  Macros: MacrosModel(sequelize),
  sequelize, // connection instance (RAW queries)
  Sequelize, // library
};

//Initialize associations
setAssociations(db);


export default db;
