import { RecipeType } from './models/recipe_type.model';
export function setAssociations(db) {
  //Add associations here



  //Day Associations
  db.Day.belongsTo(db.User, {
    foreignKey: "useremail",
    targetKey: "email",
    as: "user",
  });


  db.Day.hasMany(db.dish, {
    foreignKey: "useremail",
    targetKey: "id",
    as: "days",
  });


  //Dish Associations
  db.Dish.hasMany(db.Item, {
    foreignKey: "dishid",
    sourceKey: "id",
    as: "items",
  });

  db.Dish.belongsTo(db.Day, {
    foreignKey: "dayid",
    targetKey: "id",
  })

  //Evolution Associations
  db.Evolution.belongsTo(db.User, {
    foreignKey: "useremail",
    targetKey: "email",
    as: "user",
  });

  //Item Associations
  db.Item.belongsTo(db.Dish, {
    foreignKey: "dishid",
    targetKey: "id",
    as: "dish",
  });

  db.Item.belongsTo(db.Food, {
    foreignKey: "foodid",
    targetKey: "id",
    as: "food",
  }
  );

  //Macros Associations
  db.Macros.belongsTo(db.RecipeFood, {
    foreignKey: "recipetype",
    targetKey: "id",
    as: "recipefood",
  });

  //RecipeFood Associations
  db.RecipeFood.belongsTo(db.Food, {
    foreignKey: "foodid",
    targetKey: "id",
    as: "food",
  });

  db.RecipeFood.belongsTo(db.Recipe, {
    foreignKey: "recipeid",
    targetKey: "id",
    as: "recipe",
  });

  //Recipe Associations
  db.Recipe.belongsTo(db.RecipeType, {
    foreignKey: "type",
  
  }
  );

  db.Recipe.belongsTo(db.DayFood, {
    foreignKey: "foodofday"
  }
  );
  
  db.Recipe.hasMany(db.RecipeFood, {
    foreignKey: "recipeid",
    sourceKey: "id",
    as: "recipefood"
  });
  
  //User Associations
  db.User.belongsTo(db.RecipeType, {
    foreignKey: "typeofdiet",
  });


} 