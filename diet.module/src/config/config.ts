
/* /home/rubendev / Escritorio / Projects / NUTRIPOCKET / nutripocket / backend.package / users.module /.env */
import { config } from 'dotenv';


if (process.env.NODE_ENV !== 'production') {
  config({ path: './.development.env' });
}
else {
  config();
}


module.exports = {
  development: {
    username: process.env.DB_USERNAME!,
    password: process.env.DB_PASSWORD!,
    database: process.env.DB_NAME!,
    host: process.env.DB_HOST!,
    port: process.env.DB_PORT!,
    dialect: process.env.DIALECT,
    jwtSecret: process.env.JWT_SECRET
  },
  test: {
    username: process.env.DB_USERNAME!,
    password: process.env.DB_PASSWORD!,
    database: process.env.DB_NAME!,
    host: process.env.DB_HOST!,
    port: process.env.DB_PORT!,
    dialect: process.env.DIALECT,
    jwtSecret: process.env.JWT_SECRET
  },
  production: {
    username: process.env.DB_USERNAME!,
    password: process.env.DB_PASSWORD!,
    database: process.env.DB_NAME!,
    host: process.env.DB_HOST!,
    port: process.env.DB_PORT!,
    dialect: process.env.DIALECT,
    jwtSecret: process.env.JWT_SECRET
  }
} 
