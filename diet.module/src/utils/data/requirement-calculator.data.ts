import { Requirements } from './../../db/interfaces/requirements.interface';
import { Evolution } from './../../db/models/evolution.model';
import { User } from "../../db/models/user.model";

import { MacrosService } from 'src/api/services/macros.service';
import moment from 'moment';

export class RequirementsCalculator {
    CALORIES_PER_CARB_G = 4;
    CALORIES_PER_FAT_G = 9;
    CALORIES_PER_PROTEIN_G = 4;

    macrosService = MacrosService.getInstance();

    async calculateDayRequirements(user: User, evolution: Evolution) {
        let requirements = new Requirements();
        //Fat Loss
        if (user.goalweight < evolution.weight) {
            const basal = this.calculateDayTotalCalories(evolution, user);

            const macros = await this.macrosService.findMacrosRecipeTypeId(
                user.typeofdiet
            );
            return (requirements = new Requirements(),
                requirements = {
                    caloriesburned: basal,

                    calories_req: basal - basal * 0.15,

                    protein_req: macros[0].proteinperkg * evolution.weight,

                    fat_req: macros[0].fatperkg * evolution.weight,

                    carbs_req: Math.abs(
                        (basal -
                            basal * 0.15 -
                            macros[0].proteinperkg *
                            evolution.weight *
                            this.CALORIES_PER_PROTEIN_G -
                            macros[0].fatperkg * evolution.weight * this.CALORIES_PER_FAT_G) /
                        this.CALORIES_PER_CARB_G
                    ),
                });
        }
        //Gains
        if (user.goalweight > evolution.weight) {
            const basal = await this.calculateDayTotalCalories(evolution, user);
            const macros = await this.macrosService.findMacrosRecipeTypeId(
                user.typeofdiet
            );
            return (requirements = {
                caloriesburned: basal,
                calories_req: basal + basal * 0.15,
                protein_req: macros[0].proteinperkg * evolution.weight,
                fat_req: macros[0].fatperkg * evolution.weight,
                carbs_req: Math.abs(
                    (basal +
                        basal * 0.15 -
                        macros[0].proteinperkg *
                        evolution.weight *
                        this.CALORIES_PER_PROTEIN_G -
                        macros[0].fatperkg * evolution.weight * this.CALORIES_PER_FAT_G) /
                    this.CALORIES_PER_CARB_G
                ),
            });
        }
        //Maintain
        else {
            const basal = await this.calculateDayTotalCalories(evolution, user);
            const macros = this.macrosService.findMacrosRecipeTypeId(user.typeofdiet);
            return (requirements = {
                caloriesburned: basal,
                calories_req: basal,
                protein_req: macros[0].proteinperkg * evolution.weight,
                fat_req: macros[0].fatperkg * evolution.weight,
                carbs_req: Math.abs(
                    (basal -
                        macros[0].proteinperkg *
                        evolution.weight *
                        this.CALORIES_PER_PROTEIN_G -
                        macros[0].fatperkg * evolution.weight * this.CALORIES_PER_FAT_G) /
                    this.CALORIES_PER_CARB_G
                ),
            });
        }
    }

    //EVOLUTION	id	useremail	calreq	proteinreq	carbreq	fatreq	weight sportshoursweek	date	createdAt updatedAt
    //USER   birth	startweight	 goalweight	 caloriesdayburn typeofdietÍndice sportshoursweek genre(homre, mujer)	heightcm

    //Gasto energético (kcal/min) = 0,0175 x (5)MET x peso en kilogramos
    //Hombre: (10 x peso en kg) + (6.25 × altura en cm) - (5 × edad en años) + 5.
    //Mujer: (10 x peso en kg) + (6.25 × altura en cm) - (5 × edad en años) - 161.

    //calculate calories
    calculateBasalCalories(evolution:Evolution, user:User, minutes:number) {
        const MINUTES_IN_HOUR = 60;
        const HOURS_IN_A_DAY = 24;
        const MINUTES_IN_A_DAY = HOURS_IN_A_DAY * MINUTES_IN_HOUR;

        if (user.genre == "hombre" || user.genre == "boy") {
            let basalCaloriesDay =
                10 * evolution.weight +
                6.25 * user.heightcm -
                5 *
                (parseInt(moment().format("YYYY")) -
                    parseInt(moment(user.birth).format("YYYY"))) +
                5;
            return (basalCaloriesDay / MINUTES_IN_A_DAY) * minutes;
        } else {
            let basalCaloriesDay =
                10 * evolution.weight +
                6.25 * user.heightcm -
                5 *
                (parseInt(moment().format("YYYY")) -
                    parseInt(moment(user.birth).format("YYYY"))) -
                161;
            return (basalCaloriesDay / MINUTES_IN_A_DAY) * minutes;
        }
    }

    calculateDaySportsCalories(evolution: Evolution, minutes: number) {
        const daySportsCalories = 0.0175 * 5 * evolution.weight * minutes;
        //OK
        return daySportsCalories;
    }

    calculateDaySportsMinutes(evolution: Evolution) {
        const MINUTES_IN_HOUR = 60;
        const DAYS_IN_A_WEEK = 7;
        const minutesSportsDay =
            (evolution.sportshoursweek * MINUTES_IN_HOUR) / DAYS_IN_A_WEEK;
        //OK
        return minutesSportsDay;
    }

    calculateDayTotalCalories(evolution: Evolution, user: User) {
        const MINUTES_IN_HOUR = 60;
        const HOURS_IN_A_DAY = 24;
        const MINUTES_IN_A_DAY = HOURS_IN_A_DAY * MINUTES_IN_HOUR;
        const sportsDayMinutes = this.calculateDaySportsMinutes(evolution);
        const BasalDayMinutes = MINUTES_IN_A_DAY - sportsDayMinutes;
        const sportsDayCalories = this.calculateDaySportsCalories(
            evolution,
            sportsDayMinutes
        );
        const sportsBasalCalories = this.calculateBasalCalories(
            evolution,
            user,
            BasalDayMinutes
        );
        const totalDayCalories = sportsDayCalories + sportsBasalCalories;

        return totalDayCalories;
    }
}
