//import { Op } from "sequelize";
/*  const Op = db.Sequelize.Op;  */

import { Macros } from 'src/db/models/macros.model';
import { RecipeTypeModel } from 'src/db/models/recipe_type.model';
import DB from './../../db/config';

export class MacrosService {
    public macros = DB.Macros;

    // Singleton constructor
    private static instance: MacrosService;
    private constructor() { }
    public static getInstance(): MacrosService {
        if (!MacrosService.instance) {
            MacrosService.instance = new MacrosService();
        }
        return MacrosService.instance;
    }

    async createMacros(macros: Macros) {
        return this.macros.create(macros);
    }

    async findMacrosById(id) {
        return this.macros.findByPk(id, {
            include: {
                model: RecipeTypeModel,
                attributes: ["id", "name"],
            }
        });
    }

    async findMacrosRecipeTypeId(recipetype) {
        return this.macros.findAll({
            where: {
                recipetype: recipetype,
            },
        });
    }


    async findAllMacros() {
        return this.macros.findAll({
            include: {
                model: RecipeTypeModel,
                attributes: ["id", "name"],
            },
        });
    }

    async updateMacros(macros, id) {
        return this.macros.update(macros, {
            where: { id: id },
        });
    }


    async deleteMacros(id) {
        return this.macros.destroy({
            where: { id: id },
        });
    }
}
