import { Op } from "sequelize";
/*  const Op = db.Sequelize.Op;  */

import DB from "../../db/config";
import { Food } from "../../db/models/food.model";

export class FoodService {
  public Food = DB.Food;

  // Singleton constructor
  private static instance: FoodService;
  private constructor() {}
  public static getInstance(): FoodService {
    if (!FoodService.instance) {
      FoodService.instance = new FoodService();
    }
    return FoodService.instance;
  }

  async createUserFood(food: Food) {
    try {
      food.validated = false;
      return this.Food.create(food);
    } catch (error) {
      throw error;
    }
  }

  async createAdminFood(food: Food) {
    try {
      food.validated = true;
      return this.Food.create(food);
    } catch (error) {
      throw error;
    }
  }

  /*   async createFoods(scopesArray: Food[]) {
    return this.scopes.bulkCreate(scopesArray);
  } */

  async findAllFoods() {
    try {
      return this.Food.findAll();
    } catch (error) {
      throw error;
    }
  }

  async findValidatedFoods() {
    return this.Food.findAll({
      where: {
        validated: true,
      },
    });
  }

  async findNonValidatedFoods() {
    return this.Food.findAll({
      where: {
        validated: false,
      },
    });
  }

  async updateFood(food: Food) {
    return this.Food.update(food, {
      where: {
        id: food.id,
      },
    });
  }

  async findFoodById(id: number) {
    return this.Food.findByPk(id);
  }

  async findFoodsByNameOrNombre(name: string) {
    return this.Food.findAll({
      where: {
        [Op.or]: [
          { name: { [Op.like]: `%${name}%` } },
          { nombre: { [Op.like]: `%${name}%` } },
        ],
      },
    });
  }

  async deleteFood(id: number) {
    return this.Food.destroy({
      where: {
        id,
      },
    });
  }
}
