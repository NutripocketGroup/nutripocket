-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:3306
-- Tiempo de generación: 06-04-2022 a las 11:36:26
-- Versión del servidor: 8.0.28-0ubuntu0.20.04.3
-- Versión de PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `nutripocket`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `day`
--

CREATE TABLE `day` (
  `id` BIGINT NOT NULL,
  `date` date DEFAULT NULL,
  `useremail` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `daycaloriesburn` float DEFAULT '0',
  `daycalorieseaten` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `day`
--

INSERT INTO `day` (`id`, `date`, `useremail`, `daycaloriesburn`, `daycalorieseaten`) VALUES
(1577, '2022-01-09', 'rubensantibanezacosta902@gmail.com', 2700, 2081.11),
(1586, '2022-01-17', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1593, '2022-01-10', 'rubensantibanezacosta902@gmail.com', 0, 0),
(1594, '2022-01-11', 'rubensantibanezacosta902@gmail.com', 2700, 2157.11),
(1595, '2022-01-12', 'rubensantibanezacosta902@gmail.com', 2700, 2143.48),
(1596, '2022-01-13', 'rubensantibanezacosta902@gmail.com', 2700, 2110.66),
(1597, '2022-01-14', 'rubensantibanezacosta902@gmail.com', 2700, 2188.73),
(1598, '2022-01-15', 'rubensantibanezacosta902@gmail.com', 2700, 2175.7),
(1599, '2022-01-16', 'rubensantibanezacosta902@gmail.com', 0, 0),
(1603, '2022-03-17', 'rubensantibanezacosta902@gmail.com', 2700, 2164.45),
(1604, '2022-03-18', 'rubensantibanezacosta902@gmail.com', 2700, 2122.85),
(1605, '2022-03-19', 'rubensantibanezacosta902@gmail.com', 2700, 2166.79),
(1606, '2022-03-20', 'rubensantibanezacosta902@gmail.com', 2700, 2183.39),
(1607, '2022-03-21', 'rubensantibanezacosta902@gmail.com', 2700, 2148.87),
(1608, '2022-03-22', 'rubensantibanezacosta902@gmail.com', 2700, 2238.84),
(1609, '2022-03-16', 'rubensantibanezacosta902@gmail.com', 2700, 2159.76),
(1610, '2022-03-15', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1611, '2022-03-14', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1612, '2022-03-23', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1613, '2022-03-24', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1614, '2022-03-25', 'rubensantibanezacosta902@gmail.com', 2700, 0),
(1615, '2022-03-26', 'rubensantibanezacosta902@gmail.com', 2700, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dayfood`
--

CREATE TABLE `dayfood` (
  `id` BIGINT NOT NULL,
  `name` varchar(200) NOT NULL,
  `nombre` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `dayfood`
--

INSERT INTO `dayfood` (`id`, `name`, `nombre`) VALUES
(1, 'Breakfast', 'Desayuno'),
(2, 'Snack', 'Almuerzo'),
(3, 'Lunch', 'Comida'),
(4, 'Snack', 'Merienda'),
(5, 'Dinner', 'Cena');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `dish`
--

CREATE TABLE `dish` (
  `id` BIGINT NOT NULL,
  `dayid` BIGINT NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `dish`
--

INSERT INTO `dish` (`id`, `dayid`, `createdAt`, `updatedAt`) VALUES
(3892, 1577, '2022-01-10', '2022-01-10'),
(3893, 1577, '2022-01-10', '2022-01-10'),
(3894, 1577, '2022-01-10', '2022-01-10'),
(3895, 1577, '2022-01-10', '2022-01-10'),
(3896, 1577, '2022-01-10', '2022-01-10'),
(3937, 1586, '2022-01-10', '2022-01-10'),
(3938, 1586, '2022-01-10', '2022-01-10'),
(3939, 1586, '2022-01-10', '2022-01-10'),
(3940, 1586, '2022-01-10', '2022-01-10'),
(3941, 1586, '2022-01-10', '2022-01-10'),
(3972, 1593, '2022-01-10', '2022-01-10'),
(3973, 1593, '2022-01-10', '2022-01-10'),
(3974, 1593, '2022-01-10', '2022-01-10'),
(3975, 1593, '2022-01-10', '2022-01-10'),
(3976, 1593, '2022-01-10', '2022-01-10'),
(3977, 1594, '2022-01-10', '2022-01-10'),
(3978, 1594, '2022-01-10', '2022-01-10'),
(3979, 1594, '2022-01-10', '2022-01-10'),
(3980, 1594, '2022-01-10', '2022-01-10'),
(3981, 1594, '2022-01-10', '2022-01-10'),
(3982, 1595, '2022-01-10', '2022-01-10'),
(3983, 1595, '2022-01-10', '2022-01-10'),
(3984, 1595, '2022-01-10', '2022-01-10'),
(3985, 1595, '2022-01-10', '2022-01-10'),
(3986, 1595, '2022-01-10', '2022-01-10'),
(3987, 1596, '2022-01-10', '2022-01-10'),
(3988, 1596, '2022-01-10', '2022-01-10'),
(3989, 1596, '2022-01-10', '2022-01-10'),
(3990, 1596, '2022-01-10', '2022-01-10'),
(3991, 1596, '2022-01-10', '2022-01-10'),
(3992, 1597, '2022-01-10', '2022-01-10'),
(3993, 1597, '2022-01-10', '2022-01-10'),
(3994, 1597, '2022-01-10', '2022-01-10'),
(3995, 1597, '2022-01-10', '2022-01-10'),
(3996, 1597, '2022-01-10', '2022-01-10'),
(3997, 1598, '2022-01-10', '2022-01-10'),
(3998, 1598, '2022-01-10', '2022-01-10'),
(3999, 1598, '2022-01-10', '2022-01-10'),
(4000, 1598, '2022-01-10', '2022-01-10'),
(4001, 1598, '2022-01-10', '2022-01-10'),
(4002, 1599, '2022-01-10', '2022-01-10'),
(4003, 1599, '2022-01-10', '2022-01-10'),
(4004, 1599, '2022-01-10', '2022-01-10'),
(4005, 1599, '2022-01-10', '2022-01-10'),
(4006, 1599, '2022-01-10', '2022-01-10'),
(4022, 1603, '2022-03-16', '2022-03-16'),
(4023, 1603, '2022-03-16', '2022-03-16'),
(4024, 1603, '2022-03-16', '2022-03-16'),
(4025, 1603, '2022-03-16', '2022-03-16'),
(4026, 1603, '2022-03-16', '2022-03-16'),
(4027, 1604, '2022-03-16', '2022-03-16'),
(4028, 1604, '2022-03-16', '2022-03-16'),
(4029, 1604, '2022-03-16', '2022-03-16'),
(4030, 1604, '2022-03-16', '2022-03-16'),
(4031, 1604, '2022-03-16', '2022-03-16'),
(4032, 1605, '2022-03-16', '2022-03-16'),
(4033, 1605, '2022-03-16', '2022-03-16'),
(4034, 1605, '2022-03-16', '2022-03-16'),
(4035, 1605, '2022-03-16', '2022-03-16'),
(4036, 1605, '2022-03-16', '2022-03-16'),
(4037, 1606, '2022-03-16', '2022-03-16'),
(4038, 1606, '2022-03-16', '2022-03-16'),
(4039, 1606, '2022-03-16', '2022-03-16'),
(4040, 1606, '2022-03-16', '2022-03-16'),
(4041, 1606, '2022-03-16', '2022-03-16'),
(4042, 1607, '2022-03-16', '2022-03-16'),
(4043, 1607, '2022-03-16', '2022-03-16'),
(4044, 1607, '2022-03-16', '2022-03-16'),
(4045, 1607, '2022-03-16', '2022-03-16'),
(4046, 1607, '2022-03-16', '2022-03-16'),
(4047, 1608, '2022-03-16', '2022-03-16'),
(4048, 1608, '2022-03-16', '2022-03-16'),
(4049, 1608, '2022-03-16', '2022-03-16'),
(4050, 1608, '2022-03-16', '2022-03-16'),
(4051, 1608, '2022-03-16', '2022-03-16'),
(4052, 1609, '2022-03-16', '2022-03-16'),
(4053, 1609, '2022-03-16', '2022-03-16'),
(4054, 1609, '2022-03-16', '2022-03-16'),
(4055, 1609, '2022-03-16', '2022-03-16'),
(4056, 1609, '2022-03-16', '2022-03-16'),
(4057, 1610, '2022-03-16', '2022-03-16'),
(4058, 1610, '2022-03-16', '2022-03-16'),
(4059, 1610, '2022-03-16', '2022-03-16'),
(4060, 1610, '2022-03-16', '2022-03-16'),
(4061, 1610, '2022-03-16', '2022-03-16'),
(4062, 1611, '2022-03-16', '2022-03-16'),
(4063, 1611, '2022-03-16', '2022-03-16'),
(4064, 1611, '2022-03-16', '2022-03-16'),
(4065, 1611, '2022-03-16', '2022-03-16'),
(4066, 1611, '2022-03-16', '2022-03-16'),
(4067, 1612, '2022-03-16', '2022-03-16'),
(4068, 1612, '2022-03-16', '2022-03-16'),
(4069, 1612, '2022-03-16', '2022-03-16'),
(4070, 1612, '2022-03-16', '2022-03-16'),
(4071, 1612, '2022-03-16', '2022-03-16'),
(4072, 1613, '2022-03-16', '2022-03-16'),
(4073, 1613, '2022-03-16', '2022-03-16'),
(4074, 1613, '2022-03-16', '2022-03-16'),
(4075, 1613, '2022-03-16', '2022-03-16'),
(4076, 1613, '2022-03-16', '2022-03-16'),
(4077, 1614, '2022-03-16', '2022-03-16'),
(4078, 1614, '2022-03-16', '2022-03-16'),
(4079, 1614, '2022-03-16', '2022-03-16'),
(4080, 1614, '2022-03-16', '2022-03-16'),
(4081, 1614, '2022-03-16', '2022-03-16'),
(4082, 1615, '2022-03-16', '2022-03-16'),
(4083, 1615, '2022-03-16', '2022-03-16'),
(4084, 1615, '2022-03-16', '2022-03-16'),
(4085, 1615, '2022-03-16', '2022-03-16'),
(4086, 1615, '2022-03-16', '2022-03-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `evolution`
--

CREATE TABLE `evolution` (
  `id` BIGINT NOT NULL,
  `useremail` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `caloriesburned` float DEFAULT NULL,
  `calreq` float NOT NULL,
  `proteinreq` float DEFAULT NULL,
  `carbreq` float DEFAULT NULL,
  `fatreq` float DEFAULT NULL,
  `weight` float NOT NULL,
  `sportshoursweek` int NOT NULL,
  `date` date NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `evolution`
--

INSERT INTO `evolution` (`id`, `useremail`, `caloriesburned`, `calreq`, `proteinreq`, `carbreq`, `fatreq`, `weight`, `sportshoursweek`, `date`, `createdAt`, `updatedAt`) VALUES
(18, 'admin@admin.com', 2700, 2149.03, 233.2, 172.942, 212, 106, 7, '2021-12-25', '2021-12-25', '2021-12-25'),
(19, 'rubensantibanezacosta902@gmail.com', 2700, 2144.96, 233.2, 173.961, 212, 106, 7, '2022-01-02', '2022-01-02', '2022-01-02'),
(20, 'rubensantibanezacosta902@gmail.com', 2700, 2144.96, 233.2, 173.961, 212, 106, 7, '2022-01-02', '2022-01-02', '2022-01-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `food`
--

CREATE TABLE `food` (
  `id` BIGINT NOT NULL,
  `name` varchar(200) NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `calories100g` float NOT NULL,
  `protein100g` float NOT NULL,
  `carbs100g` float NOT NULL,
  `fat100g` float NOT NULL,
  `saturatedfat100g` float DEFAULT NULL,
  `sugar100g` float DEFAULT NULL,
  `fiber100g` float DEFAULT NULL,
  `salt100g` float DEFAULT NULL,
  `doseg` float NOT NULL DEFAULT '1',
  `validated` tinyint DEFAULT '0',
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `food`
--

INSERT INTO `food` (`id`, `name`, `nombre`, `calories100g`, `protein100g`, `carbs100g`, `fat100g`, `saturatedfat100g`, `sugar100g`, `fiber100g`, `salt100g`, `doseg`, `validated`, `createdAt`, `updatedAt`) VALUES
(1, 'Chicken breast', 'Pechuga de pollo', 110, 23.09, 0, 1.24, 0, 0, 0, 0, 10, 1, '2021-12-20', '2021-12-20'),
(2, 'Brown Rice', 'Arroz Integral', 362, 7.5, 76.17, 2.68, 0, 0, 0, 0, 10, 1, '2021-12-20', '2021-12-20'),
(3, 'Skimmed plain yogurt 0%', 'Yogur natural desnatado 0%', 45, 5.4, 5.6, 0.1, 0, 0, 0, 0, 125, 1, '2021-12-20', '2021-12-20'),
(4, 'Skimmed milk', 'Leche Desnatada', 34, 3.2, 4.7, 0.25, 0, 0, 0, 0, 10, 1, '2021-12-21', '2021-12-21'),
(5, 'Oatmeal', 'Avena', 389, 16.89, 66.27, 6.9, 0, 0, 0, 0, 5, 1, '2021-12-21', '2021-12-21'),
(6, 'Raw almonds', 'Alemdras Crudas', 578, 21.26, 19.74, 50.64, 0, 0, 0, 0, 1, 1, '2021-12-21', '2021-12-21'),
(7, 'Cream of zucchini', 'Crema de Calabacín', 68, 2.46, 5.81, 3.95, 0, 0, 0, 0, 20, 0, '2021-12-21', '2021-12-21'),
(8, 'Grilled hake', 'Merluza a la plancha', 132, 21.38, 0.41, 4.38, 0, 0, 0, 0, 10, 1, '2021-12-21', '2021-12-21'),
(9, 'azúcar', 'azúcar', 350, 0, 100, 0, 0, 100, 0, 0, 1, 0, '2022-01-08', '2022-01-08'),
(10, 'prueba', 'prueba', 200, 10, 10, 10, 0, 10, 0, 0, 10, 0, '2022-01-08', '2022-01-08'),
(11, 'aasd', 'aasd', 100, 100, 100, 10, 10, 1, 0, 0, 15, 0, '2022-01-08', '2022-01-08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item`
--

CREATE TABLE `item` (
  `id` BIGINT NOT NULL,
  `dosex` float NOT NULL,
  `dishid` BIGINT NOT NULL DEFAULT '1',
  `foodid` BIGINT DEFAULT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `item`
--

INSERT INTO `item` (`id`, `dosex`, `dishid`, `foodid`, `createdAt`, `updatedAt`) VALUES
(11343, 51, 3892, 4, '2022-01-10', '2022-01-10'),
(11344, 1, 3892, 5, '2022-01-10', '2022-01-10'),
(11345, 41, 3892, 6, '2022-01-10', '2022-01-10'),
(11346, 4, 3893, 3, '2022-01-10', '2022-01-10'),
(11347, 1, 3894, 1, '2022-01-10', '2022-01-10'),
(11348, 3, 3894, 2, '2022-01-10', '2022-01-10'),
(11349, 2, 3894, 3, '2022-01-10', '2022-01-10'),
(11350, 6, 3894, 3, '2022-01-10', '2022-01-10'),
(11351, 56, 3895, 6, '2022-01-10', '2022-01-10'),
(11355, 12, 3896, 8, '2022-01-10', '2022-01-10'),
(11356, 11, 3896, 7, '2022-01-10', '2022-01-10'),
(11357, 4, 3896, 3, '2022-01-10', '2022-01-10'),
(11524, 40, 3972, 4, '2022-01-10', '2022-01-10'),
(11525, 2, 3972, 5, '2022-01-10', '2022-01-10'),
(11526, 44, 3972, 6, '2022-01-10', '2022-01-10'),
(11527, 4, 3973, 3, '2022-01-10', '2022-01-10'),
(11528, 7, 3974, 1, '2022-01-10', '2022-01-10'),
(11529, 2, 3974, 2, '2022-01-10', '2022-01-10'),
(11530, 3, 3974, 3, '2022-01-10', '2022-01-10'),
(11531, 4, 3974, 3, '2022-01-10', '2022-01-10'),
(11532, 56, 3975, 6, '2022-01-10', '2022-01-10'),
(11533, 10, 3976, 8, '2022-01-10', '2022-01-10'),
(11534, 4, 3976, 3, '2022-01-10', '2022-01-10'),
(11535, 13, 3976, 7, '2022-01-10', '2022-01-10'),
(11536, 42, 3977, 4, '2022-01-10', '2022-01-10'),
(11537, 1, 3977, 5, '2022-01-10', '2022-01-10'),
(11538, 46, 3977, 6, '2022-01-10', '2022-01-10'),
(11539, 4, 3978, 3, '2022-01-10', '2022-01-10'),
(11540, 6, 3979, 1, '2022-01-10', '2022-01-10'),
(11541, 2, 3979, 2, '2022-01-10', '2022-01-10'),
(11542, 5, 3979, 3, '2022-01-10', '2022-01-10'),
(11543, 4, 3979, 3, '2022-01-10', '2022-01-10'),
(11544, 56, 3980, 6, '2022-01-10', '2022-01-10'),
(11545, 10, 3981, 8, '2022-01-10', '2022-01-10'),
(11546, 9, 3981, 7, '2022-01-10', '2022-01-10'),
(11547, 5, 3981, 3, '2022-01-10', '2022-01-10'),
(11548, 44, 3982, 4, '2022-01-10', '2022-01-10'),
(11549, 1, 3982, 5, '2022-01-10', '2022-01-10'),
(11550, 45, 3982, 6, '2022-01-10', '2022-01-10'),
(11551, 4, 3983, 3, '2022-01-10', '2022-01-10'),
(11552, 16, 3984, 1, '2022-01-10', '2022-01-10'),
(11553, 3, 3984, 2, '2022-01-10', '2022-01-10'),
(11554, 2, 3984, 3, '2022-01-10', '2022-01-10'),
(11555, 4, 3984, 3, '2022-01-10', '2022-01-10'),
(11556, 56, 3985, 6, '2022-01-10', '2022-01-10'),
(11557, 15, 3986, 8, '2022-01-10', '2022-01-10'),
(11558, 13, 3986, 7, '2022-01-10', '2022-01-10'),
(11559, 3, 3986, 3, '2022-01-10', '2022-01-10'),
(11560, 43, 3987, 4, '2022-01-10', '2022-01-10'),
(11561, 1, 3987, 5, '2022-01-10', '2022-01-10'),
(11562, 46, 3987, 6, '2022-01-10', '2022-01-10'),
(11563, 4, 3988, 3, '2022-01-10', '2022-01-10'),
(11564, 17, 3989, 1, '2022-01-10', '2022-01-10'),
(11565, 5, 3989, 2, '2022-01-10', '2022-01-10'),
(11566, 2, 3989, 3, '2022-01-10', '2022-01-10'),
(11567, 2, 3989, 3, '2022-01-10', '2022-01-10'),
(11568, 56, 3990, 6, '2022-01-10', '2022-01-10'),
(11569, 21, 3991, 8, '2022-01-10', '2022-01-10'),
(11570, 15, 3991, 7, '2022-01-10', '2022-01-10'),
(11571, 1, 3991, 3, '2022-01-10', '2022-01-10'),
(11572, 44, 3992, 4, '2022-01-10', '2022-01-10'),
(11573, 1, 3992, 5, '2022-01-10', '2022-01-10'),
(11574, 45, 3992, 6, '2022-01-10', '2022-01-10'),
(11575, 4, 3993, 3, '2022-01-10', '2022-01-10'),
(11576, 2, 3994, 1, '2022-01-10', '2022-01-10'),
(11577, 4, 3994, 2, '2022-01-10', '2022-01-10'),
(11578, 2, 3994, 3, '2022-01-10', '2022-01-10'),
(11579, 7, 3994, 3, '2022-01-10', '2022-01-10'),
(11580, 56, 3995, 6, '2022-01-10', '2022-01-10'),
(11581, 20, 3996, 8, '2022-01-10', '2022-01-10'),
(11582, 16, 3996, 7, '2022-01-10', '2022-01-10'),
(11583, 1, 3996, 3, '2022-01-10', '2022-01-10'),
(11584, 40, 3997, 4, '2022-01-10', '2022-01-10'),
(11585, 2, 3997, 5, '2022-01-10', '2022-01-10'),
(11586, 44, 3997, 6, '2022-01-10', '2022-01-10'),
(11587, 4, 3998, 3, '2022-01-10', '2022-01-10'),
(11588, 2, 3999, 1, '2022-01-10', '2022-01-10'),
(11589, 2, 3999, 2, '2022-01-10', '2022-01-10'),
(11590, 8, 3999, 3, '2022-01-10', '2022-01-10'),
(11591, 2, 3999, 3, '2022-01-10', '2022-01-10'),
(11592, 56, 4000, 6, '2022-01-10', '2022-01-10'),
(11593, 17, 4001, 8, '2022-01-10', '2022-01-10'),
(11594, 15, 4001, 7, '2022-01-10', '2022-01-10'),
(11595, 2, 4001, 3, '2022-01-10', '2022-01-10'),
(11596, 35, 4002, 4, '2022-01-10', '2022-01-10'),
(11597, 2, 4002, 5, '2022-01-10', '2022-01-10'),
(11598, 47, 4002, 6, '2022-01-10', '2022-01-10'),
(11599, 4, 4003, 3, '2022-01-10', '2022-01-10'),
(11600, 4, 4004, 1, '2022-01-10', '2022-01-10'),
(11601, 1, 4004, 2, '2022-01-10', '2022-01-10'),
(11602, 5, 4004, 3, '2022-01-10', '2022-01-10'),
(11603, 5, 4004, 3, '2022-01-10', '2022-01-10'),
(11604, 56, 4005, 6, '2022-01-10', '2022-01-10'),
(11605, 21, 4006, 8, '2022-01-10', '2022-01-10'),
(11606, 15, 4006, 7, '2022-01-10', '2022-01-10'),
(11607, 1, 4006, 3, '2022-01-10', '2022-01-10'),
(11639, 46, 4022, 4, '2022-03-16', '2022-03-16'),
(11640, 1, 4022, 5, '2022-03-16', '2022-03-16'),
(11641, 44, 4022, 6, '2022-03-16', '2022-03-16'),
(11642, 4, 4023, 3, '2022-03-16', '2022-03-16'),
(11643, 29, 4024, 1, '2022-03-16', '2022-03-16'),
(11644, 3, 4024, 2, '2022-03-16', '2022-03-16'),
(11645, 3, 4024, 3, '2022-03-16', '2022-03-16'),
(11646, 1, 4024, 3, '2022-03-16', '2022-03-16'),
(11647, 56, 4025, 6, '2022-03-16', '2022-03-16'),
(11648, 12, 4026, 8, '2022-03-16', '2022-03-16'),
(11649, 11, 4026, 7, '2022-03-16', '2022-03-16'),
(11650, 4, 4026, 3, '2022-03-16', '2022-03-16'),
(11651, 40, 4027, 4, '2022-03-16', '2022-03-16'),
(11652, 2, 4027, 5, '2022-03-16', '2022-03-16'),
(11653, 44, 4027, 6, '2022-03-16', '2022-03-16'),
(11654, 4, 4028, 3, '2022-03-16', '2022-03-16'),
(11655, 5, 4029, 2, '2022-03-16', '2022-03-16'),
(11656, 2, 4029, 3, '2022-03-16', '2022-03-16'),
(11657, 2, 4029, 3, '2022-03-16', '2022-03-16'),
(11658, 19, 4029, 1, '2022-03-16', '2022-03-16'),
(11659, 56, 4030, 6, '2022-03-16', '2022-03-16'),
(11660, 15, 4031, 8, '2022-03-16', '2022-03-16'),
(11661, 12, 4031, 7, '2022-03-16', '2022-03-16'),
(11662, 3, 4031, 3, '2022-03-16', '2022-03-16'),
(11663, 32, 4032, 4, '2022-03-16', '2022-03-16'),
(11664, 1, 4032, 5, '2022-03-16', '2022-03-16'),
(11665, 52, 4032, 6, '2022-03-16', '2022-03-16'),
(11666, 4, 4033, 3, '2022-03-16', '2022-03-16'),
(11667, 13, 4034, 1, '2022-03-16', '2022-03-16'),
(11668, 3, 4034, 2, '2022-03-16', '2022-03-16'),
(11669, 6, 4034, 3, '2022-03-16', '2022-03-16'),
(11670, 1, 4034, 3, '2022-03-16', '2022-03-16'),
(11671, 56, 4035, 6, '2022-03-16', '2022-03-16'),
(11672, 14, 4036, 8, '2022-03-16', '2022-03-16'),
(11673, 14, 4036, 7, '2022-03-16', '2022-03-16'),
(11674, 3, 4036, 3, '2022-03-16', '2022-03-16'),
(11675, 32, 4037, 4, '2022-03-16', '2022-03-16'),
(11676, 1, 4037, 5, '2022-03-16', '2022-03-16'),
(11677, 52, 4037, 6, '2022-03-16', '2022-03-16'),
(11678, 4, 4038, 3, '2022-03-16', '2022-03-16'),
(11679, 22, 4039, 1, '2022-03-16', '2022-03-16'),
(11680, 4, 4039, 2, '2022-03-16', '2022-03-16'),
(11681, 2, 4039, 3, '2022-03-16', '2022-03-16'),
(11682, 3, 4039, 3, '2022-03-16', '2022-03-16'),
(11683, 56, 4040, 6, '2022-03-16', '2022-03-16'),
(11684, 20, 4041, 8, '2022-03-16', '2022-03-16'),
(11685, 16, 4041, 7, '2022-03-16', '2022-03-16'),
(11686, 1, 4041, 3, '2022-03-16', '2022-03-16'),
(11687, 39, 4042, 4, '2022-03-16', '2022-03-16'),
(11688, 1, 4042, 5, '2022-03-16', '2022-03-16'),
(11689, 48, 4042, 6, '2022-03-16', '2022-03-16'),
(11690, 4, 4043, 3, '2022-03-16', '2022-03-16'),
(11691, 28, 4044, 1, '2022-03-16', '2022-03-16'),
(11692, 6, 4044, 2, '2022-03-16', '2022-03-16'),
(11693, 1, 4044, 3, '2022-03-16', '2022-03-16'),
(11694, 1, 4044, 3, '2022-03-16', '2022-03-16'),
(11695, 56, 4045, 6, '2022-03-16', '2022-03-16'),
(11696, 12, 4046, 8, '2022-03-16', '2022-03-16'),
(11697, 11, 4046, 7, '2022-03-16', '2022-03-16'),
(11698, 4, 4046, 3, '2022-03-16', '2022-03-16'),
(11699, 35, 4047, 4, '2022-03-16', '2022-03-16'),
(11700, 2, 4047, 5, '2022-03-16', '2022-03-16'),
(11701, 47, 4047, 6, '2022-03-16', '2022-03-16'),
(11702, 4, 4048, 3, '2022-03-16', '2022-03-16'),
(11703, 6, 4049, 1, '2022-03-16', '2022-03-16'),
(11704, 4, 4049, 2, '2022-03-16', '2022-03-16'),
(11705, 6, 4049, 3, '2022-03-16', '2022-03-16'),
(11706, 3, 4049, 3, '2022-03-16', '2022-03-16'),
(11707, 56, 4050, 6, '2022-03-16', '2022-03-16'),
(11708, 15, 4051, 8, '2022-03-16', '2022-03-16'),
(11709, 13, 4051, 7, '2022-03-16', '2022-03-16'),
(11710, 3, 4051, 3, '2022-03-16', '2022-03-16'),
(11711, 51, 4052, 4, '2022-03-16', '2022-03-16'),
(11712, 1, 4052, 5, '2022-03-16', '2022-03-16'),
(11713, 41, 4052, 6, '2022-03-16', '2022-03-16'),
(11714, 4, 4053, 3, '2022-03-16', '2022-03-16'),
(11715, 2, 4054, 2, '2022-03-16', '2022-03-16'),
(11716, 7, 4054, 3, '2022-03-16', '2022-03-16'),
(11717, 1, 4054, 3, '2022-03-16', '2022-03-16'),
(11718, 11, 4054, 1, '2022-03-16', '2022-03-16'),
(11719, 56, 4055, 6, '2022-03-16', '2022-03-16'),
(11720, 20, 4056, 8, '2022-03-16', '2022-03-16'),
(11721, 16, 4056, 7, '2022-03-16', '2022-03-16'),
(11722, 1, 4056, 3, '2022-03-16', '2022-03-16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `macros`
--

CREATE TABLE `macros` (
  `id` BIGINT NOT NULL,
  `recipetype` BIGINT NOT NULL,
  `proteinperkg` float DEFAULT NULL,
  `fatperkg` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `macros`
--

INSERT INTO `macros` (`id`, `recipetype`, `proteinperkg`, `fatperkg`) VALUES
(1, 3, 2.2, 2),
(2, 2, 2, 1.5),
(3, 1, 1.2, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe`
--

CREATE TABLE `recipe` (
  `id` BIGINT NOT NULL,
  `type` BIGINT NOT NULL,
  `foodofday` BIGINT NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `recipe`
--

INSERT INTO `recipe` (`id`, `type`, `foodofday`, `createdAt`, `updatedAt`) VALUES
(1, 3, 3, '2021-12-21', '2021-12-21'),
(2, 3, 1, '2021-12-21', '2021-12-21'),
(3, 3, 2, '2021-12-21', '2021-12-21'),
(4, 3, 4, '2021-12-21', '2021-12-21'),
(5, 3, 5, '2021-12-21', '2021-12-21');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe_food`
--

CREATE TABLE `recipe_food` (
  `id` BIGINT NOT NULL,
  `foodid` BIGINT NOT NULL,
  `recipeid` BIGINT NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `recipe_food`
--

INSERT INTO `recipe_food` (`id`, `foodid`, `recipeid`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 2),
(5, 5, 2),
(6, 6, 2),
(7, 3, 3),
(8, 6, 4),
(9, 8, 5),
(10, 7, 5),
(11, 3, 5),
(12, 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `recipe_type`
--

CREATE TABLE `recipe_type` (
  `id` BIGINT NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `recipe_type`
--

INSERT INTO `recipe_type` (`id`, `name`) VALUES
(1, 'Standard'),
(2, 'Low Carb'),
(3, 'Keto');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `scopes`
--

CREATE TABLE `scopes` (
  `id` BIGINT NOT NULL,
  `name` varchar(50) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `scopes`
--

INSERT INTO `scopes` (`id`, `name`, `role`) VALUES
(1, 'create:day', 'admin'),
(2, 'create:days', 'admin'),
(3, 'update:day', 'admin'),
(4, 'read:day', 'admin'),
(5, 'delete:day', 'admin'),
(6, 'read:evolution', 'admin'),
(7, 'create:evolution', 'admin'),
(8, 'update:evolution', 'admin'),
(9, 'delete:evolution', 'admin'),
(10, 'create:food', 'admin'),
(11, 'read:food', 'admin'),
(12, 'update:food', 'admin'),
(13, 'delete:food', 'admin'),
(14, 'create:item', 'admin'),
(15, 'read:item', 'admin'),
(16, 'update:item', 'admin'),
(17, 'delete:item', 'admin'),
(18, 'create:recipe', 'admin'),
(19, 'read:recipe', 'admin'),
(20, 'update:recipe', 'admin'),
(21, 'delete:recipe', 'admin'),
(22, 'create:user', 'admin'),
(23, 'read:user', 'admin'),
(24, 'update:user', 'admin'),
(25, 'delete:user', 'admin'),
(26, 'create:day', 'user'),
(27, 'read:day', 'user'),
(28, 'update:day', 'user'),
(29, 'delete:day', 'user'),
(30, 'create:evolution', 'user'),
(31, 'read:evolution', 'user'),
(32, 'update:evolution', 'user'),
(33, 'delete:evolution', 'user'),
(34, 'create:food', 'user'),
(35, 'read:food', 'user'),
(36, 'update:food', 'user'),
(37, 'delete:food', 'user'),
(38, 'create:item', 'user'),
(39, 'read:item', 'user'),
(40, 'update:item', 'user'),
(41, 'delete:item', 'user'),
(42, 'create:user', 'user'),
(43, 'read:user', 'user'),
(44, 'update:user', 'user'),
(45, 'delete:user', 'user'),
(46, 'create:recipe', 'user'),
(47, 'read:recipe', 'user'),
(48, 'update:recipe', 'user'),
(49, 'delete:recipe', 'user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `email` varchar(500) NOT NULL,
  `password` varchar(500) DEFAULT NULL,
  `name` varchar(500) NOT NULL,
  `surname` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `birth` date DEFAULT NULL,
  `genre` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `heightcm` float DEFAULT NULL,
  `startweight` float DEFAULT NULL,
  `goalweight` float DEFAULT NULL,
  `typeofdiet` BIGINT DEFAULT NULL,
  `role` varchar(50) NOT NULL DEFAULT 'user',
  `createdAt` date DEFAULT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`email`, `password`, `name`, `surname`, `birth`, `genre`, `heightcm`, `startweight`, `goalweight`, `typeofdiet`, `role`, `createdAt`, `updatedAt`) VALUES
('admin@admin.com', '1234', 'admin', 'apellidos', '1987-10-09', 'boy', 186, 103, 87, 3, 'admin', '2021-12-17', '2021-12-17'),
('rubensantibanezacosta902@gmail.com', '$2b$10$Wd26KACWeURnaH1BXtPZFupO5H8hbgMZdN9P26.uOOUewm4BtcP3m', 'Rubén', NULL, '1987-10-09', 'boy', 186, 103, 87, 3, 'user', '2022-01-02', '2022-01-02');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `userscopes`
--

CREATE TABLE `userscopes` (
  `id` BIGINT NOT NULL,
  `useremail` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `scopeid` BIGINT NOT NULL,
  `createdAt` date NOT NULL,
  `updatedAt` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `userscopes`
--

INSERT INTO `userscopes` (`id`, `useremail`, `scopeid`, `createdAt`, `updatedAt`) VALUES
(2, 'rubensantibanezacosta902@gmail.com', 26, '2022-01-02', '2022-01-02'),
(3, 'rubensantibanezacosta902@gmail.com', 27, '2022-01-02', '2022-01-02'),
(4, 'rubensantibanezacosta902@gmail.com', 28, '2022-01-02', '2022-01-02'),
(5, 'rubensantibanezacosta902@gmail.com', 29, '2022-01-02', '2022-01-02'),
(6, 'rubensantibanezacosta902@gmail.com', 30, '2022-01-02', '2022-01-02'),
(7, 'rubensantibanezacosta902@gmail.com', 31, '2022-01-02', '2022-01-02'),
(8, 'rubensantibanezacosta902@gmail.com', 32, '2022-01-02', '2022-01-02'),
(9, 'rubensantibanezacosta902@gmail.com', 33, '2022-01-02', '2022-01-02'),
(10, 'rubensantibanezacosta902@gmail.com', 34, '2022-01-02', '2022-01-02'),
(11, 'rubensantibanezacosta902@gmail.com', 35, '2022-01-02', '2022-01-02'),
(12, 'rubensantibanezacosta902@gmail.com', 36, '2022-01-02', '2022-01-02'),
(13, 'rubensantibanezacosta902@gmail.com', 37, '2022-01-02', '2022-01-02'),
(14, 'rubensantibanezacosta902@gmail.com', 38, '2022-01-02', '2022-01-02'),
(15, 'rubensantibanezacosta902@gmail.com', 39, '2022-01-02', '2022-01-02'),
(16, 'rubensantibanezacosta902@gmail.com', 40, '2022-01-02', '2022-01-02'),
(17, 'rubensantibanezacosta902@gmail.com', 41, '2022-01-02', '2022-01-02'),
(18, 'rubensantibanezacosta902@gmail.com', 42, '2022-01-02', '2022-01-02'),
(19, 'rubensantibanezacosta902@gmail.com', 43, '2022-01-02', '2022-01-02'),
(20, 'rubensantibanezacosta902@gmail.com', 44, '2022-01-02', '2022-01-02'),
(21, 'rubensantibanezacosta902@gmail.com', 45, '2022-01-02', '2022-01-02'),
(22, 'rubensantibanezacosta902@gmail.com', 46, '2022-01-02', '2022-01-02'),
(23, 'rubensantibanezacosta902@gmail.com', 47, '2022-01-02', '2022-01-02'),
(24, 'rubensantibanezacosta902@gmail.com', 48, '2022-01-02', '2022-01-02'),
(25, 'rubensantibanezacosta902@gmail.com', 49, '2022-01-02', '2022-01-02');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `day`
--
ALTER TABLE `day`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_day_email` (`useremail`);

--
-- Indices de la tabla `dayfood`
--
ALTER TABLE `dayfood`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `dish`
--
ALTER TABLE `dish`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_dish_day` (`dayid`);

--
-- Indices de la tabla `evolution`
--
ALTER TABLE `evolution`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_evolution_user` (`useremail`);

--
-- Indices de la tabla `food`
--
ALTER TABLE `food`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_dish_item` (`dishid`),
  ADD KEY `FK_food_item` (`foodid`);

--
-- Indices de la tabla `macros`
--
ALTER TABLE `macros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_macros_recipeType` (`recipetype`);

--
-- Indices de la tabla `recipe`
--
ALTER TABLE `recipe`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_recipe_recypetype` (`type`),
  ADD KEY `FK_recipe_dayFood` (`foodofday`);

--
-- Indices de la tabla `recipe_food`
--
ALTER TABLE `recipe_food`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_food_recipefood` (`foodid`),
  ADD KEY `FK_recipe_recipefood` (`recipeid`);

--
-- Indices de la tabla `recipe_type`
--
ALTER TABLE `recipe_type`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `scopes`
--
ALTER TABLE `scopes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`email`),
  ADD KEY `FK_user_diet_type` (`typeofdiet`);

--
-- Indices de la tabla `userscopes`
--
ALTER TABLE `userscopes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Fk_userscope_user` (`useremail`),
  ADD KEY `Fk_userscope_scope` (`scopeid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `day`
--
ALTER TABLE `day`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1616;

--
-- AUTO_INCREMENT de la tabla `dayfood`
--
ALTER TABLE `dayfood`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `dish`
--
ALTER TABLE `dish`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4087;

--
-- AUTO_INCREMENT de la tabla `evolution`
--
ALTER TABLE `evolution`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT de la tabla `food`
--
ALTER TABLE `food`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT de la tabla `item`
--
ALTER TABLE `item`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11723;

--
-- AUTO_INCREMENT de la tabla `macros`
--
ALTER TABLE `macros`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `recipe`
--
ALTER TABLE `recipe`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `recipe_food`
--
ALTER TABLE `recipe_food`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `recipe_type`
--
ALTER TABLE `recipe_type`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `scopes`
--
ALTER TABLE `scopes`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `userscopes`
--
ALTER TABLE `userscopes`
  MODIFY `id` BIGINT NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `day`
--
ALTER TABLE `day`
  ADD CONSTRAINT `FK_day_email` FOREIGN KEY (`useremail`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `dish`
--
ALTER TABLE `dish`
  ADD CONSTRAINT `FK_dish_day` FOREIGN KEY (`dayid`) REFERENCES `day` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `evolution`
--
ALTER TABLE `evolution`
  ADD CONSTRAINT `FK_evolution_user` FOREIGN KEY (`useremail`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `FK_dish_item` FOREIGN KEY (`dishid`) REFERENCES `dish` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_food_item` FOREIGN KEY (`foodid`) REFERENCES `food` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `macros`
--
ALTER TABLE `macros`
  ADD CONSTRAINT `FK_macros_recipeType` FOREIGN KEY (`recipetype`) REFERENCES `recipe_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recipe`
--
ALTER TABLE `recipe`
  ADD CONSTRAINT `FK_recipe_dayFood` FOREIGN KEY (`foodofday`) REFERENCES `dayfood` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_recipe_recypetype` FOREIGN KEY (`type`) REFERENCES `recipe_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `recipe_food`
--
ALTER TABLE `recipe_food`
  ADD CONSTRAINT `FK_food_recipefood` FOREIGN KEY (`foodid`) REFERENCES `food` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_recipe_recipefood` FOREIGN KEY (`recipeid`) REFERENCES `recipe` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FK_user_diet_type` FOREIGN KEY (`typeofdiet`) REFERENCES `recipe_type` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `userscopes`
--
ALTER TABLE `userscopes`
  ADD CONSTRAINT `Fk_userscope_scope` FOREIGN KEY (`scopeid`) REFERENCES `scopes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `Fk_userscope_user` FOREIGN KEY (`useremail`) REFERENCES `user` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
